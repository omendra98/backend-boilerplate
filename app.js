const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');

const config = require('./config');

const influencerPhoneNumberAuthRoutes = require('./routes/influencer/auth/phone-number-auth');
const influencerInstagramAuthRoutes = require('./routes/influencer/auth/instagram-auth');
const influencerTruecallerAuthRoutes = require('./routes/influencer/auth/truecaller-auth');
const influencerProfileRoutes = require('./routes/influencer/profile');
const influencerBookmarkCampaignRoutes = require('./routes/influencer/campaign/bookmark-campaign');
const influencerGetSubscribedBrandsCampaignsRoutes = require('./routes/influencer/campaign/get-subscribed-brands-campaigns');
const influencerSearchRoutes = require('./routes/influencer/search');
const influencerGetCampaigns = require('./routes/influencer/campaign/get-campaigns');
const influencerGetBrands = require('./routes/influencer/brand/get-brands');
const influencerBookmarkBrandRoutes = require('./routes/influencer/brand/bookmark-brand');
const influencerGetCampaignsByStatus = require('./routes/influencer/applicant/get-campaigns-by-status');
const influencerAddBankAccount = require('./routes/influencer/wallet/add-bank-account');
const influencerAddUPIAccount = require('./routes/influencer/wallet/add-upi-account');

const companyAuthRoutes = require('./routes/company/auth');
const companyProfileRoutes = require('./routes/company/profile');
const companyPasswordResetRoutes = require('./routes/company/password_reset');
const companySearchRoutes = require('./routes/company/search');
const companyBookmarkRoutes = require('./routes/company/bookmark');
const brandRoutes = require('./routes/company/brand');
const companyGetInfluencersRoutes = require('./routes/company/get-influencers');

const campaignRoutes = require('./routes/campaign');
const campaignApplicantsRoutes = require('./routes/company/campaign-applicants');
const applyCampaignRoutes = require('./routes/campaign/apply-campaign');

const influencerLibraryRoutes = require('./routes/library/influencer');
const companyLibraryRoutes = require('./routes/library/company');

const adminCategoryRoutes = require('./routes/admin/category');
const adminReferralRoutes = require('./routes/admin/referral');

const utilRoutes = require('./routes/util/image-upload');
const getLocationsRoute = require('./routes/util/get-locations');

const orderRoutes = require('./routes/orders/order');
const paymentRoutes = require('./routes/orders/payment');

const notificationRoutes = require('./routes/notification/notification');

const app = express();

// view engine setup
app.set('views', './views');
app.set('view engine', 'pug');

// Use JSON body parser
app.use(bodyParser.json());

app.use(express.static('public'));

// for parsing application/xwww-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Enable CORS requests
app.use(cors());

// Use HTTP request console logging
app.use(morgan('combined'));

// Connect to MongoDB Atlas instance
mongoose
    .connect(config.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('Connected to MongoDB instance');
    })
    .catch((error) => {
        console.log(error);
    });

app.use('/influencer', influencerPhoneNumberAuthRoutes);
app.use('/influencer', influencerInstagramAuthRoutes);
app.use('/influencer', influencerTruecallerAuthRoutes);
app.use('/influencer', influencerProfileRoutes);
app.use('/influencer', influencerBookmarkCampaignRoutes);
app.use('/influencer', influencerGetSubscribedBrandsCampaignsRoutes);
app.use('/influencer', influencerSearchRoutes);
app.use('/influencer', influencerGetCampaigns);
app.use('/influencer', influencerGetBrands);
app.use('/influencer', influencerBookmarkBrandRoutes);
app.use('/influencer', influencerGetCampaignsByStatus);
app.use('/influencer', influencerAddBankAccount);
app.use('/influencer', influencerAddUPIAccount);

app.use('/company', companyAuthRoutes);
app.use('/company', companyProfileRoutes);
app.use('/company', companyPasswordResetRoutes);
app.use('/company', companySearchRoutes);
app.use('/company', companyBookmarkRoutes);
app.use('/company', brandRoutes);
app.use('/company', companyGetInfluencersRoutes);

app.use('/campaign', campaignRoutes);
app.use('/campaign', applyCampaignRoutes);
app.use('/campaign/applicant', campaignApplicantsRoutes);

app.use('/library', influencerLibraryRoutes);
app.use('/library', companyLibraryRoutes);

app.use('/order', orderRoutes);
app.use('/payment', paymentRoutes);

app.use('/notification', notificationRoutes);

app.use('/admin', adminCategoryRoutes);
app.use('/admin', adminReferralRoutes);

app.use(utilRoutes);
app.use('/util', getLocationsRoute);

// Configure Express app
const appConfig = app.listen(config.PORT, config.HOST, () => {
    console.log(`Listening on ${appConfig.address().address}:${appConfig.address().port}`);
});
