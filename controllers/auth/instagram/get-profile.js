const axios = require('axios');

const calculateEngagement = (profile) => {
    const numberofFollowers = profile.edge_followed_by.count;
    let sum = 0.0;
    for (let edge of profile.edge_felix_video_timeline.edges) {
        sum += edge.edge_liked_by.count;
    }
    if (profile.edge_felix_video_timeline.count) {
        profile.engagement = (parseFloat(sum) / (numberofFollowers * profile.edge_felix_video_timeline.count)) * 100.0;
    } else {
        profile.engagement = 0.0;
    }
};

const getProfile = (username) => {
    const url = `https://www.instagram.com/${username}/`;
    return axios
        .get(url)
        .then((response) => {
            const re = new RegExp('window._sharedData = (.*);</script>');
            const profile = JSON.parse(re.exec(response.data)[1]).entry_data.ProfilePage[0].graphql.user;
            calculateEngagement(profile);
            return profile;
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

const assignProfile = (profile, influencer) => {
    influencer.platforms.instagram.username = profile.username;
    influencer.platforms.instagram.numberOfFollowers = profile.edge_followed_by.count;
    influencer.platforms.instagram.numberOfFollowings = profile.edge_follow.count;
    influencer.platforms.instagram.fullName = profile.full_name;
    influencer.platforms.instagram.profilePicture = profile.profile_pic_url;
    influencer.platforms.instagram.profilePictureHD = profile.profile_pic_url_hd;
    influencer.platforms.instagram.engagement = profile.engagement;

    influencer.markModified('platforms');
};

module.exports = {
    getProfile: getProfile,
    assignProfile: assignProfile,
};
