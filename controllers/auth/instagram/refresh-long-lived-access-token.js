const axios = require('axios');

const refreshLongLivedAccessToken = (longLivedAccessToken) => {
    const url = `https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=${longLivedAccessToken}`;
    return axios.get(url);
};

module.exports = refreshLongLivedAccessToken;
