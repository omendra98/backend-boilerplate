const jwt = require('jsonwebtoken');

const config = require('../../config');

const authMiddleware = (userType) => {
    return (req, res, next) => {
        jwt.verify(req.headers['authorization'], config.JWT_SECRET, (error, decoded_token) => {
            if (error) {
                res.status(401).json({ error: true, message: [error.message] });
            } else {
                if (userType && decoded_token.userType !== userType) {
                    res.status(401).json({ error: true, message: ['User not authorized'] });
                } else {
                    req.user = decoded_token.userId;
                    next();
                }
            }
        });
    };
};

module.exports = authMiddleware;
