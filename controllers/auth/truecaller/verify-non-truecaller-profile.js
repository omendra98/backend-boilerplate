const axios = require('axios');

const { TRUECALLER_APP_KEY } = require('../../../config');

const verifyNonTruecallerProfile = (accessToken) => {
    const url = `https://api4.truecaller.com/v1/otp/installation/phoneNumberDetail/${accessToken}`;
    return axios
        .get(url, {
            headers: {
                appkey: TRUECALLER_APP_KEY,
            },
        })
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = verifyNonTruecallerProfile;
