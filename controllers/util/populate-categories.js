const Category = require('../../models/category');

const populateCategories = async (categoryIds) => {
    const categories = [];
    for (let categoryId of categoryIds) {
        await Category.findById(categoryId)
            .then((category) => {
                if (category != null) {
                    categories.push({ id: category._id, name: category.name, description: category.description, icon: category.icon });
                }
            })
            .catch((error) => {
                return Promise.reject(error);
            });
    }
    return categories;
};

module.exports = populateCategories;
