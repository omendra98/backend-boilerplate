const { validationResult } = require('express-validator');

const mongoose = require('mongoose');

const Library = require('../../models/library');

const middlewares = {};

// function to get all the available content for user based on multiple paramters
middlewares.viewAllContents = (req, res, next) => {
    // checking for validation error
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const findFilter = {};
        findFilter['paidBy'] = { $in: [mongoose.Types.ObjectId(req.user), null] };
        const status = ['All', 'Tagged', 'Open', 'Paid', 'Unpaid', 'Bookmarked']; // all the allowed status

        // pagination paramters
        const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        const size = req.query.size ? parseInt(req.query.size) : 20;

        if (pageNo <= 0) return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

        // checking for the status of the contents that are required
        if (status.includes(req.params.tag) !== true) return res.status(400).send({ error: true, message: ['Invalid url'] });
        if (req.params.tag === 'Paid') {
            findFilter['paid'] = true;
            findFilter['tag.companyId'] = { $in: [mongoose.Types.ObjectId(req.user), null] };
        }
        if (req.params.tag === 'Unpaid') {
            findFilter['paid'] = false;
            findFilter['tag.companyId'] = { $in: [mongoose.Types.ObjectId(req.user), null] };
        }
        if (req.params.tag === 'All') findFilter['tag.companyId'] = { $in: [mongoose.Types.ObjectId(req.user), null] };
        if (req.params.tag === 'Tagged') findFilter['tag.companyId'] = mongoose.Types.ObjectId(req.user);
        if (req.params.tag === 'Open') findFilter['tag.companyId'] = null;
        if (req.params.tag === 'Bookmarked') {
            findFilter['tag.companyId'] = { $in: [mongoose.Types.ObjectId(req.user), null] };
            findFilter['bookmarkedBy'] = mongoose.Types.ObjectId(req.user);
        }

        // fetching the contents based on the given paramters
        Library.aggregate()
            .match(findFilter)
            .project({
                influencerId: true,
                url: true,
                price: true,
                caption: true,
                isBookmarked: {
                    $eq: [
                        {
                            $size: {
                                $ifNull: [{ $setIntersection: ['$bookmarkedBy', [mongoose.Types.ObjectId(req.user)]] }, []],
                            },
                        },
                        1,
                    ],
                },
                paidPartnershipAllowed: true,
                paid: true,
            })
            .limit(size)
            .skip(size * (pageNo - 1))
            .exec((err, contents) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });

                return res.status(200).send({ error: false, result: { contents: contents } });
            });
    } else {
        return res.status(400).send({ error: true, message: [errors.array()] });
    }
};

// function to bookmark a content
middlewares.bookmarkContent = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // fetching the content using the contentId
        Library.findById(req.body.contentId, (err, content) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!content) return res.status(400).send({ error: true, message: ['invalid contentId'] });

            // checking if the user is having access to bookmark the content or not
            if (content.tag.companyId === req.user || (content.tag.companyId === null && content.paid === false)) {
                // checking if the user has already bookmarked this content or not
                if (content.bookmarkedBy.includes(req.user)) return res.status(400).send({ error: true, message: ['Already bookmarked'] });
                content.bookmarkedBy.push(req.user);
                content.save((err) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    return res.status(200).send({ error: false, result: { message: 'content bookmarked successfully' } });
                });
            } else {
                return res.status(401).send({ error: true, message: ['not authorized to perform this operation'] });
            }
        });
    } else {
        return res.status(400).send({ error: true, message: [errors.message] });
    }
};

// function to remove the bookmark from the content
middlewares.deleteBookmarkContent = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // fetching the content using the given contentId
        Library.findById(req.body.contentId, (err, content) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!content) return res.status(400).send({ error: true, message: ['invalid contentId'] });

            // checking if bookmark is already there or not
            if (content.bookmarkedBy.includes(req.user)) {
                // removing the user from bookmark array
                content.bookmarkedBy.splice(req.user, 1);
                content.save((err) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    return res.status(200).send({ error: false, result: { message: 'Bookmark to the content is successfully removed' } });
                });
            } else {
                return res.status(400).send({ error: true, message: ['content is not bookmarked'] });
            }
        });
    } else {
        return res.status(400).send({ error: true, message: [errors.message] });
    }
};

module.exports = middlewares;
