const { validationResult } = require('express-validator');

const Influencer = require('../../models/influencer');

const searchMiddlewares = {};

searchMiddlewares.search = (req, res, next) => {
    var query = {}; // no filter

    if (req.query.name) {
        query.name = req.query.name;
    }

    // getting the influencer based on above filters
    Influencer.find(query, { _id: 1, name: 1, city: 1, gender: 1 }, (err, users) => {
        if (err) return res.status(500).send({ error: true, message: ['There is a problem finding the influencer.'] });

        return res.status(200).send({ error: false, data: users });
    });
};

module.exports = searchMiddlewares;
