const Brand = require('../../../models/brand');

const mongoose = require('mongoose');

const middlewares = {};

// function to fetch brands for influencers with filtering, pagination and sorting features
middlewares.getAllBrands = async (req, res, next) => {
    // getting all the parameters
    const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
    const size = req.query.size ? parseInt(req.query.size) : 20;

    if (pageNo <= 0) return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

    // fetching the brands based on provided paramters
    await Brand.aggregate(
        [
            {
                $project: {
                    _id: true,
                    name: true,
                    logo: true,
                    about: true,
                    coverUrl: true,
                    rating: true,
                    campaignCount: { $size: '$$ROOT.campaigns' },
                    isSubscribed: {
                        $eq: [
                            {
                                $size: {
                                    $ifNull: [{ $setIntersection: ['$subscribedBy', [mongoose.Types.ObjectId(req.user)]] }, []],
                                },
                            },
                            1,
                        ],
                    },
                },
            },
            {
                $skip: size * (pageNo - 1),
            },
            {
                $limit: size,
            },
            {
                $sort: {
                    campaignCount: -1,
                },
            },
        ],
        (err, brands) => {
            if (err) return res.status(400).send({ error: true, message: [err.message] });
            return res.status(200).send({ error: false, result: { brands: brands } });
        }
    );
};

module.exports = middlewares;
