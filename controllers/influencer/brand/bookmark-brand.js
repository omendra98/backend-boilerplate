const { validationResult } = require('express-validator');
const mongoose = require('mongoose');

const Brand = require('../../../models/brand');

const middlewares = {};

// function to bookmark a brand for an influencer
middlewares.bookmarkBrand = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // finding the brand using the given id to bookmark
        Brand.findById(req.body.id).then((brand) => {
            if (brand != null) {
                // if brand exists, add the influencer id in the brand subscriber list
                brand.subscribedBy.push(req.user);
                // save the brand object
                brand.save((err) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    return res.status(200).send({ error: false, result: ['Successfully bookmarked the brand.'] });
                });
            } else {
                res.status(400).json({ error: true, message: ['Brand does not exist.'] });
            }
        });
    } else {
        res.status(400).json({ error: true, message: ['Validation error in user fields.'] });
    }
};

// function to remove the influencer from the bookmark list of brands
middlewares.removeBookmarkedBrand = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // finding the brand using the given id
        Brand.findById(req.query.id, (err, brand) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!brand) return res.status(400).send({ error: true, message: ['No such brand exists'] });

            // removing the influencer id from the subscriber list
            const influencerIndex = brand.subscribedBy.indexOf(req.user);
            brand.subscribedBy.splice(influencerIndex, 1);
            brand.save((err) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });

                return res.status(200).send({ error: false, result: ['successfully removed the bookmarked brand.'] });
            });
        });
    } else {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    }
};

// function to get all the bookmarked brands of an influencer
middlewares.getBookmarkedBrands = (req, res, next) => {
    // getting all the parameters
    const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
    const size = req.query.size ? parseInt(req.query.size) : 20;

    if (pageNo <= 0) return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

    // making the query to find the bookmarked brands
    Brand.aggregate()
        .match({ subscribedBy: { $all: [mongoose.Types.ObjectId(req.user)] } })
        .project({
            _id: true,
            companyId: true,
            name: true,
            logo: true,
            coverUrl: true,
            about: true,
            rating: true,
        })
        .limit(size)
        .skip(size * (pageNo - 1))
        .exec((err, brands) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });

            return res.status(200).send({ error: false, result: { brands: brands } });
        });
};

module.exports = middlewares;
