const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const Influencer = require('../../../models/influencer');
const Constant = require('../../../models/constant');

const exchangeInstagramToken = require('../../auth/instagram/exchange-instagram-token');
const getUsername = require('../../auth/instagram/get-username');

const config = require('../../../config');

const addCredits = require('../addCredits');

const middlewares = {};

middlewares.signinInstagram = (req, res, next) => {
    exchangeInstagramToken(req.body.code)
        .then((instagram_object) => {
            // getProfile(instagram_object.userId, instagram_object.accessToken)
            //     .then((profile) => {
            getUsername(instagram_object.userId, instagram_object.accessToken)
                .then((username) => {
                    Influencer.findOne({ 'platforms.instagram.userId': instagram_object.userId }).then(async (influencer) => {
                        let registeredInfluencer = null;
                        let result;
                        if (influencer != null) {
                            registeredInfluencer = influencer;
                        } else {
                            registeredInfluencer = new Influencer();
                            registeredInfluencer.platforms.instagram = {};
                            registeredInfluencer.platforms.instagram.userId = instagram_object.userId;
                            if (String(req.body.referee) != 'null') {
                                result = await addCredits(req.body.referee, registeredInfluencer)
                                    .then(async () => {
                                        return await Constant.findOne({ name: 'referralAmountForReferred' })
                                            .then((constant) => {
                                                registeredInfluencer.referee = mongoose.Types.ObjectId(req.body.referee);
                                                registeredInfluencer.credits += parseInt(constant.value);
                                                return null;
                                            })
                                            .catch((error) => {
                                                return Promise.reject(error);
                                            });
                                    })
                                    .catch((error) => {
                                        return Promise.reject(error);
                                    });
                                console.log(result);
                            }
                        }
                        if (result != null) {
                            res.status(500).json({ error: true, message: ['Error caused for referral.'] });
                        } else {
                            registeredInfluencer.platforms.instagram.accessToken = instagram_object.accessToken;
                            registeredInfluencer.platforms.instagram.accessTokenExpiry = instagram_object.expiry;
                            registeredInfluencer.platforms.instagram.username = username;
                            registeredInfluencer.markModified('platforms');
                            console.log(registeredInfluencer);
                            // assignProfile(profile, registeredInfluencer);
                            registeredInfluencer.save().then((influencer) => {
                                const jwtToken = jwt.sign(
                                    {
                                        userId: influencer._id,
                                        userType: 'influencer',
                                    },
                                    config.JWT_SECRET
                                );
                                res.status(200).json({ error: false, result: { auth: true, token: jwtToken, userId: influencer._id } });
                            });
                        }
                    });
                })
                .catch((error) => {
                    console.log(error);
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            console.log(error);
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.addInstagram = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        exchangeInstagramToken(req.body.code)
            .then((instagram_object) => {
                // getProfile(instagram_object.userId, instagram_object.accessToken)
                //     .then((profile) => {
                getUsername(instagram_object.userId, instagram_object.accessToken)
                    .then((username) => {
                        Influencer.findById(req.user)
                            .then((influencer) => {
                                if (influencer != null) {
                                    influencer.platforms.instagram = {};
                                    influencer.platforms.instagram.userId = instagram_object.userId;
                                    influencer.platforms.instagram.accessToken = instagram_object.accessToken;
                                    influencer.platforms.instagram.accessTokenExpiry = instagram_object.expiry;
                                    influencer.platforms.instagram.username = username;
                                    // assignProfile(profile, influencer);
                                    influencer.save().then((record) => {
                                        res.status(200).json({ error: false, message: ['Instagram account successfully added.'] });
                                    });
                                } else {
                                    res.status(400).json({ error: true, message: ['Influencer does not exists.'] });
                                }
                            })
                            .catch((error) => {
                                res.status(400).json({ error: true, message: [error.message] });
                            });
                    })
                    .catch((error) => {
                        res.status(400).json({ error: true, message: [error.message] });
                    });
            })
            .catch((error) => {
                res.status(400).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    }
};

module.exports = middlewares;
