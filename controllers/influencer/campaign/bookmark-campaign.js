const { validationResult } = require('express-validator');

const Influencer = require('../../../models/influencer');
const Campaign = require('../../../models/campaign');

const { fetchCampaign } = require('../../campaign/fetch-campaign');

const middlewares = {};

middlewares.bookmarkCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Campaign.findById(req.body.id).then((campaign) => {
            if (campaign.status === 'Active') {
                Influencer.findById(req.user)
                    .then((influencer) => {
                        if (influencer != null) {
                            influencer.bookmarkedCampaigns.push(req.body.id);
                            influencer
                                .save()
                                .then((record) => {
                                    res.status(200).json({ error: false, message: ['Successfully bookmarked the campaign.'] });
                                })
                                .catch((error) => {
                                    res.status(500).json({ error: true, message: [error.message] });
                                });
                        } else {
                            res.status(400).json({ error: true, message: ['No such influencer found.'] });
                        }
                    })
                    .catch((error) => {
                        res.status(500).json({ error: true, message: [error.message] });
                    });
            } else {
                res.status(400).json({ error: true, message: ['You cannot bookmark this campaign.'] });
            }
        });
    } else {
        res.status(400).json({ error: true, message: ['Validation error in user fields.'] });
    }
};

middlewares.removeBookmarkedCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Influencer.findById(req.user)
            .then((influencer) => {
                if (influencer != null) {
                    const campaignIndex = influencer.bookmarkedCampaigns.indexOf(req.query.id);
                    influencer.bookmarkedCampaigns.splice(campaignIndex, 1);
                    influencer
                        .save()
                        .then((record) => {
                            res.status(200).json({ error: false, message: ['Successfully removed the bookmarked campaign.'] });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: ['No such influencer found.'] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    }
};

middlewares.getBookmarkedCampaigns = (req, res, next) => {
    Influencer.findById(req.user)
        .then(async (influencer) => {
            if (influencer != null) {
                const bookmarkedCampaigns = [];
                for (let campaignId of influencer.bookmarkedCampaigns) {
                    let { result, isError, isServerError } = await fetchCampaign(campaignId, true);
                    if (!isError && !isServerError) {
                        bookmarkedCampaigns.push(result);
                    }
                }
                res.status(200).json({ error: false, result: bookmarkedCampaigns });
            } else {
                res.status(400).json({ error: true, message: ['No such influencer found.'] });
            }
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

module.exports = middlewares;
