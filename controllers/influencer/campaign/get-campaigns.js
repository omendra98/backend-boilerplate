const { validationResult } = require('express-validator');

const { fetchCampaign } = require('../../campaign/fetch-campaign');

const Category = require('../../../models/category');
const Brand = require('../../../models/brand');
const Campaign = require('../../../models/campaign');
const Influencer = require('../../../models/influencer');
const Applicant = require('../../../models/applicant');

const middlewares = {};

// function to fetch the all the campaings with the queried categories
middlewares.getCampaignsByCategories = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // pagination parameters
        const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        const size = req.query.size ? parseInt(req.query.size) : 20;

        if (pageNo <= 0) return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

        Influencer.findById(req.user)
            .then((influencer) => {
                if (influencer != null) {
                    const appliedCampaigns = [];
                    Applicant.find({ influencer: req.user }).exec(async (err, applicants) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        for (const applicant of applicants) {
                            appliedCampaigns.push(applicant.campaign);
                        }

                        let filteredCampaigns = [];
                        // fetching all the campaigns with active status
                        Campaign.find({ status: 'Active', _id: { $nin: appliedCampaigns } })
                            .select({
                                _id: 1,
                                activatedOn: 1,
                                status: 1,
                                brief: 1,
                                criteria: 1,
                                budget: 1,
                                startDate: 1,
                                endDate: 1,
                                company: 1,
                                brand: 1,
                            })
                            .limit(size)
                            .skip(size * (pageNo - 1))
                            .sort(req.query.sortBy)
                            .exec(async (err, campaigns) => {
                                if (err) return res.status(500).send({ error: true, message: [err.message] });
                                if (!campaigns) return res.status(400).send({ error: true, message: ['No such campaigns'] });

                                if (req.query.categories) {
                                    for (const campaign of campaigns) {
                                        // filtering the campaigns using the categories
                                        for (const category of req.query.categories) {
                                            if (campaign.criteria.categories.includes(category)) {
                                                filteredCampaigns.push(campaign);
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    filteredCampaigns = campaigns;
                                }

                                const resultCampaigns = [];
                                for (const campaign of filteredCampaigns) {
                                    let result;
                                    if (influencer.bookmarkedCampaigns.includes(campaign._id)) {
                                        result = await fetchCampaign(campaign._id, true);
                                    } else {
                                        result = await fetchCampaign(campaign._id, false);
                                    }
                                    if (!result.isError && !result.isServerError) {
                                        resultCampaigns.push(result.result);
                                    }
                                }
                                return res.status(200).send({ error: false, result: resultCampaigns });
                            });
                    });
                } else {
                    res.status(400).json({ error: true, message: [error.message] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    }
};

// function to fetch campaigns for influencers with filtering, pagination and sorting features
middlewares.getAllCampaigns = (req, res, next) => {
    // getting all the parameters
    const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
    const size = req.query.size ? parseInt(req.query.size) : 20;
    const categories = req.query.categories ? [...req.query.categories] : [];

    if (pageNo <= 0) return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

    // fetching the campaigns based on provided paramters
    Influencer.findById(req.user)
        .then(async (influencer) => {
            if (influencer != null) {
                const appliedCampaigns = [];
                await Applicant.find({ influencer: req.user }).exec(async (err, applicants) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    for (const applicant of applicants) {
                        appliedCampaigns.push(applicant.campaign);
                    }
                    let query;
                    if (categories.length == 0) {
                        query = { status: 'Active', _id: { $nin: appliedCampaigns } };
                    } else {
                        query = { status: 'Active', _id: { $nin: appliedCampaigns }, 'criteria.categories': { $in: categories } };
                    }
                    await Campaign.find(query)
                        .limit(size)
                        .skip(size * (pageNo - 1))
                        .sort(req.query.sortBy)
                        .select({ _id: 1 })
                        .exec(async (err, campaigns) => {
                            if (err) return res.status(400).send({ error: true, message: [err.message] });
                            else {
                                const resultCampaigns = [];
                                for (const campaign of campaigns) {
                                    let result;
                                    if (influencer.bookmarkedCampaigns.includes(campaign._id)) {
                                        result = await fetchCampaign(campaign._id, true);
                                    } else {
                                        result = await fetchCampaign(campaign._id, false);
                                    }
                                    if (!result.isError && !result.isServerError) {
                                        resultCampaigns.push(result.result);
                                    }
                                }
                                return res.status(200).send({ error: false, result: resultCampaigns });
                            }
                        });
                });
            } else {
                res.status(400).json({ error: true, message: ['Influencer not found.'] });
            }
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

// function to search for campaigns
middlewares.searchCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        const size = req.query.size ? parseInt(req.query.size) : 20;
        const categoryPage = req.query.categorypage ? parseInt(req.query.categorypage) : 1;
        const categorySize = req.query.categorysize ? parseInt(req.query.categorysize) : 10;
        const brandPage = req.query.brandpage ? parseInt(req.query.brandpage) : 1;
        const brandSize = req.query.brandsize ? parseInt(req.query.brandsize) : 20;

        if (pageNo <= 0) return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

        Category.find({
            name: { $regex: req.query.query, $options: 'i' },
        })
            .select({ _id: 1, name: 1 })
            .limit(categorySize)
            .skip(categorySize * (categoryPage - 1))
            .exec((err, validCategories) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                const validCategoriesList = validCategories.map((category) => category._id);

                Brand.find({
                    name: { $regex: req.query.query, $options: 'i' },
                })
                    .select({ _id: 1, name: 1 })
                    .limit(brandSize)
                    .skip(brandSize * (brandPage - 1))
                    .exec((err, validBrands) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });
                        const validBrandsList = validBrands.map((brand) => brand._id);

                        Campaign.find({
                            status: 'Active',
                            $or: [
                                { 'brief.title': { $regex: req.query.query, $options: 'i' } },
                                { brand: { $in: validBrandsList } },
                                { 'criteria.categories': { $all: validCategoriesList } },
                            ],
                        })
                            .limit(size)
                            .exec((err, campaigns) => {
                                if (err) return res.status(500).send({ error: true, message: [err.message] });

                                return res.status(200).send({ error: false, result: { campaigns: campaigns } });
                            });
                    });
            });

        // Campaign.find(findFilter)
        // .limit(size)
        // .skip(size * (pageNo - 1))
        // .exec((err, campaigns) => {
        //     if (err) return res.status(500).send({ error: true, message: [err.message] });

        //     return res.status(200).send({ error: false, result: {campaigns: campaigns} });
        // })
    } else {
        return res.status(400).send({ error: true, message: [errors.array()] });
    }
};

// functions to get all the campaigns of a specific brand of the user
middlewares.getCampaignsByBrandId = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Brand.findById(req.query.id)
            .then(async (brand) => {
                if (brand != null) {
                    Influencer.findById(req.user)
                        .then(async (influencer) => {
                            let campaigns = [];
                            for (const campaignId of brand.campaigns) {
                                await Campaign.find({ _id: campaignId, status: 'Active' })
                                    .then(async (campaign) => {
                                        if (campaign.length != 0) {
                                            let campaign;
                                            if (influencer.bookmarkedCampaigns.includes(campaignId)) {
                                                campaign = await fetchCampaign(campaignId, true);
                                            } else {
                                                campaign = await fetchCampaign(campaignId, false);
                                            }
                                            campaigns.push(campaign.result);
                                        }
                                    })
                                    .catch((error) => {
                                        res.status(500).json({ error: true, message: [error.message] });
                                    });
                            }
                            res.status(200).json({ error: false, result: campaigns });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: ['No such brand exists.'] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: ['validation error in user fields.'] });
    }
};

module.exports = middlewares;
