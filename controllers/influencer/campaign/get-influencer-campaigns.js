const { validationResult } = require('express-validator');

const { fetchCampaign } = require('../../campaign/fetch-campaign');

const Influencer = require('../../../models/influencer');
const Applicant = require('../../../models/applicant');

const middlewares = {};

middlewares.getInfluencerCampaigns = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Influencer.findById(req.user)
            .then(async (influencer) => {
                if (influencer != null) {
                    for (let campaignId of influencer.campaigns) {
                        const campaigns = [];
                        await Applicant.findOne({ campaign: campaignId, influencer: req.user })
                            .then(async (applicant) => {
                                if (applicant != null) {
                                    if (applicant.status == req.params.status) {
                                        if (influencer.bookmarkedCampaigns.includes(campaignId)) {
                                            campaigns.push(await fetchCampaign(campaignId, true));
                                        } else {
                                            campaigns.push(await fetchCampaign(campaignId, false));
                                        }
                                    }
                                } else {
                                    res.status(400).json({ error: true, message: ['No such applicant exists.'] });
                                }
                            })
                            .catch((error) => {
                                res.status(500).json({ error: true, message: [error.message] });
                            });
                        res.status(200).json({ error: false, result: campaigns });
                    }
                } else {
                    res.status(400).json({ error: true, message: ['No such influencer exists.'] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    }
};

module.exports = middlewares;
