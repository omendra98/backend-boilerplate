const Campaign = require('../../../models/campaign');
const Influencer = require('../../../models/influencer');
const Brand = require('../../../models/brand');

const { fetchCampaignData } = require('../../campaign/fetch-campaign');
const Company = require('../../../models/company');

const middlewares = {};

middlewares.getSubscribedBrandsCampaigns = (req, res, next) => {
    Influencer.findById(req.user)
        .then((influencer) => {
            Brand.find({ subscribedBy: req.user })
                .then(async (brands) => {
                    const subscribedBrandsCampaigns = [];
                    for (const brand of brands) {
                        for (const campaignId of brand.campaigns) {
                            await Campaign.findById(campaignId)
                                .then(async (campaign) => {
                                    if (campaign.status === 'Active') {
                                        await Company.findById(campaign.company)
                                            .then(async (company) => {
                                                if (influencer.bookmarkedCampaigns.includes(campaignId)) {
                                                    subscribedBrandsCampaigns.push(await fetchCampaignData(campaign, brand, company, true));
                                                } else {
                                                    subscribedBrandsCampaigns.push(
                                                        await fetchCampaignData(campaign, brand, company, false)
                                                    );
                                                }
                                            })
                                            .catch((error) => {
                                                console.log(error.message);
                                            });
                                    }
                                })
                                .catch((error) => {
                                    console.log(error.message);
                                });
                        }
                    }
                    res.status(200).json({ error: false, result: subscribedBrandsCampaigns });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

module.exports = middlewares;
