const { validationResult } = require('express-validator');

const Influencer = require('../../models/influencer');
const Category = require('../../models/category');
const populateCategories = require('../../controllers/util/populate-categories');

const profileMiddlewares = {};

const fetchProfile = async (influencer) => {
    return {
        id: influencer._id,
        name: influencer.name,
        dob: influencer.dob,
        gender: influencer.gender,
        city: influencer.city,
        country: influencer.country,
        email: influencer.email,
        phoneNumber: influencer.phoneNumber,
        platforms: influencer.platforms,
        categories: await populateCategories(influencer.categories),
        fundsAccounts: influencer.fundsAccounts,
        defaultAccount: influencer.defaultAccount,
        newsletter: influencer.newsletter,
        createdAt: influencer.createdAt,
        updatedAt: influencer.updatedAt,
    };
};

const assignProfile = async (influencer, profile) => {
    influencer.name = profile.name;
    influencer.dob = profile.dob;
    influencer.gender = profile.gender;
    influencer.city = profile.city;
    influencer.country = profile.country;
    influencer.email = profile.email;
    influencer.phoneNumber = profile.phoneNumber;
    influencer.categories = profile.categories;
    influencer.newsletter = newsletter;
};

profileMiddlewares.getProfile = (req, res, next) => {
    Influencer.findById(req.user)
        .then(async (influencer) => {
            if (influencer != null) {
                res.status(200).json({ error: false, result: await fetchProfile(influencer) });
            } else {
                res.status(400).json({ error: true, message: ['Influencer not found.'] });
            }
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

profileMiddlewares.saveProfile = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        Influencer.findById(req.user)
            .then((influencer) => {
                if (influencer != null) {
                    assignProfile(influencer, req.body);
                    influencer
                        .save()
                        .then(() => {
                            res.status(201).json({ error: false, message: ['Success'] });
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: ['Influencer not found.'] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    }
};

profileMiddlewares.getCategories = (req, res, next) => {
    Category.find()
        .then(async (categories) => {
            Influencer.findById(req.user)
                .then(async (influencer) => {
                    if (influencer != null) {
                        const finalCategories = [];
                        for (let category of categories) {
                            const influencerCategory = {
                                id: category._id,
                                name: category.name,
                                description: category.description,
                                icon: category.icon,
                            };
                            if (influencer.categories.includes(category._id) && req.query.selected !== 'false') {
                                influencerCategory.selected = true;
                                finalCategories.push(influencerCategory);
                            }
                            if (!influencer.categories.includes(category._id) && req.query.selected !== 'true') {
                                influencerCategory.selected = false;
                                finalCategories.push(influencerCategory);
                            }
                        }
                        res.status(200).json({ error: false, result: finalCategories });
                    } else {
                        res.status(400).json({ error: true, message: ['Influencer not found.'] });
                    }
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

profileMiddlewares.saveCategories = (req, res, next) => {
    Influencer.findById(req.user)
        .then((influencer) => {
            if (influencer != null) {
                influencer.categories = req.body.categoryIds;
                influencer
                    .save()
                    .then((record) => {
                        res.status(201).json({ error: false, message: ['Success'] });
                    })
                    .catch((error) => {
                        res.status(500).json({ error: true, message: [error.message] });
                    });
            } else {
                res.status(400).json({ error: true, message: ['Influencer not found.'] });
            }
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

module.exports = profileMiddlewares;
