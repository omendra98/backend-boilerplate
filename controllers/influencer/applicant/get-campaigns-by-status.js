const { validationResult } = require('express-validator');

const Applicant = require('../../../models/applicant');
const Influencer = require('../../../models/influencer');
const { fetchCampaign } = require('../../campaign/fetch-campaign');

const middlewares = {};

middlewares.getCampaignsByStatus = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Applicant.find({ influencer: req.user, status: req.query.status })
            .then((applicants) => {
                Influencer.findById(req.user)
                    .then(async (influencer) => {
                        const campaigns = [];
                        for (let applicant of applicants) {
                            if (influencer.bookmarkedCampaigns.includes(applicant.campaign)) {
                                const { result, isError, isServerError } = await fetchCampaign(applicant.campaign, true);
                                if (!isError && !isServerError) {
                                    campaigns.push(result);
                                }
                            } else {
                                const { result, isError, isServerError } = await fetchCampaign(applicant.campaign, false);
                                if (!isError && !isServerError) {
                                    campaigns.push(result);
                                }
                            }
                        }
                        res.status(200).json({ error: false, result: campaigns });
                    })
                    .catch((error) => {
                        res.status(500).json({ error: true, message: [error.message] });
                    });
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: ['Validation error in user fields.'] });
    }
};

module.exports = middlewares;
