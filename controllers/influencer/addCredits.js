const mongoose = require('mongoose');

const Constant = require('../../models/constant');
const Influencer = require('../../models/influencer');

const addCredits = async (referee, registeredInfluencer) => {
    return await Influencer.findById(referee)
        .then((refereeInfluencer) => {
            if (refereeInfluencer != null) {
                Constant.findOne({ name: 'referralAmountForReferrer' })
                    .then((constant) => {
                        refereeInfluencer.credits += parseInt(constant.value);
                        refereeInfluencer.save().then((record) => {
                            return;
                        });
                    })
                    .catch((error) => {
                        console.log(error);
                        return Promise.reject('Server error occured');
                    });
            } else {
                return Promise.reject('Referee not found');
            }
        })
        .catch((error) => {
            console.log(error);
            return Promise.reject('Server error occured');
        });
};

module.exports = addCredits;
