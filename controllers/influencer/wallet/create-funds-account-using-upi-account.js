const { axiosInstance } = require('../../../razorpay/razorpay');

const createFundsAccountUsingUPIAccount = (contactId, upiId) => {
    return axiosInstance
        .post('/fund_accounts', {
            contact_id: contactId,
            account_type: 'vpa',
            vpa: {
                address: upiId,
            },
        })
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = createFundsAccountUsingUPIAccount;
