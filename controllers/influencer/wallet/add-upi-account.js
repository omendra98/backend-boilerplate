const { validationResult } = require('express-validator');

const Influencer = require('../../../models/influencer');

const createContact = require('./create-contact');
const createFundsAccount = require('./create-funds-account-using-upi-account');

const middlewares = {};

middlewares.addUPIAccount = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Influencer.findById(req.user).then((influencer) => {
            const contact = Number(influencer.phoneNumber.slice(3));
            createContact(influencer.name, influencer.email, contact, 'Influencer')
                .then((contact) => {
                    createFundsAccount(contact.id, req.body.upiId)
                        .then((fundsAccount) => {
                            if (!influencer.fundsAccounts.includes(fundsAccount)) {
                                influencer.fundsAccounts.push(fundsAccount);
                                influencer.defaultAccount = influencer.fundsAccounts.length - 1;
                                influencer
                                    .save()
                                    .then((record) => {
                                        res.status(200).json({ error: false, message: ['UPI Account added.'] });
                                    })
                                    .catch((error) => {
                                        res.status(500).json({ error: true, message: [error.message] });
                                    });
                            } else {
                                res.status(200).json({ error: false, message: ['UPI Account already exists.'] });
                            }
                        })
                        .catch((error) => {
                            res.status(500).json({ error: true, message: [error] });
                        });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error] });
                });
        });
    } else {
        res.status(400).json({ error: true, message: ['Validation errors.'] });
    }
};

module.exports = middlewares;
