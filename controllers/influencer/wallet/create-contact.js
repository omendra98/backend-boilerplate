const { axiosInstance } = require('../../../razorpay/razorpay');

const createContact = (name, email, contact, type) => {
    return axiosInstance
        .post('/contacts', {
            name: name,
            email: email,
            contact: contact,
            type: type,
        })
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = createContact;
