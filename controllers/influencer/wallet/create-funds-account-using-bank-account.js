const { axiosInstance } = require('../../../razorpay/razorpay');

const createFundsAccountUsingBankAccount = (contactId, name, ifsc, accountNumber) => {
    return axiosInstance
        .post('/fund_accounts', {
            contact_id: contactId,
            account_type: 'bank_account',
            bank_account: {
                name: name,
                ifsc: ifsc,
                account_number: accountNumber,
            },
        })
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = createFundsAccountUsingBankAccount;
