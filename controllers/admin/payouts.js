const { axiosInstance } = require('../../razorpay/razorpay');

const payouts = (accountNumber, fundAccountId, amount, currency, mode, purpose, queued, narration, notes) => {
    return axiosInstance
        .post('/payouts', {
            account_number: accountNumber,
            fund_account_id: fundAccountId,
            amount: amount,
            currency: currency,
            mode: mode,
            purpose: purpose,
            queue_if_low_balance: queued,
            narration: narration,
            notes: notes,
        })
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return Promise.reject(error);
        });
};

module.exports = payouts;
