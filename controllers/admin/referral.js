const { validationResult } = require('express-validator');

const Constant = require('../../models/constant');

const middlewares = {};

middlewares.getReferralAmountForReferrer = (req, res, next) => {
    Constant.findOne({ name: 'referralAmountForReferrer' })
        .then((constant) => {
            res.status(200).json({ error: false, referralAmount: parseInt(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getReferralAmountForReferred = (req, res, next) => {
    Constant.findOne({ name: 'referralAmountForReferred' })
        .then((constant) => {
            res.status(200).json({ error: false, referralAmount: parseInt(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getReferralPercentage = (req, res, next) => {
    Constant.findOne({ name: 'referralPercentage' })
        .then((constant) => {
            res.status(200).json({ error: false, referralPercentage: parseFloat(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.getReferralMaxAmount = (req, res, next) => {
    Constant.findOne({ name: 'referralMaxAmount' })
        .then((constant) => {
            res.status(200).json({ error: false, referralMaxAmount: parseInt(constant.value) });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveReferralAmountForReferrer = (req, res, next) => {
    Constant.findOne({ name: 'referralAmountForReferrer' })
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'referralAmountForReferrer';
            }
            constant.value = String(req.body.referralAmountForReferrer);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: 'Referral mount saved for referrer' });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveReferralAmountForReferred = (req, res, next) => {
    Constant.findOne({ name: 'referralAmountForReferred' })
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'referralAmountForReferred';
            }
            constant.value = String(req.body.referralAmountForReferred);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: 'Referral Amount saved for referred' });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveReferralPercentage = (req, res, next) => {
    Constant.findOne({ name: 'referralPercentage' })
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'referralPercentage';
            }
            constant.value = String(req.body.referralPercentage);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: 'Referral Percentage saved.' });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

middlewares.saveReferralMaxAmount = (req, res, next) => {
    Constant.findOne({ name: 'referralMaxAmount' })
        .then((constant) => {
            if (constant == null) {
                constant = new Constant();
                constant.name = 'referralMaxAmount';
            }
            constant.value = String(req.body.referralMaxAmount);
            constant
                .save()
                .then((record) => {
                    res.status(200).json({ error: false, result: 'Referral Max Amount saved.' });
                })
                .catch((error) => {
                    res.status(500).json({ error: true, message: [error.message] });
                });
        })
        .catch((error) => {
            res.status(500).json({ error: true, message: [error.message] });
        });
};

module.exports = middlewares;
