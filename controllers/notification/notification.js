const { validationResult } = require('express-validator');

const Notification = require('../../models/notification');
const Subscription = require('../../models/subscriber');

const mongoose = require('mongoose');

const fcm = require('../../firebase/fcm');

const middlewares = {};

// function to get all the notifications
middlewares.getNotifications = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // pagination parameters
        const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
        const size = req.query.size ? parseInt(req.query.size) : 20;

        if (pageNo <= 0) return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

        const findFilter = {};
        findFilter['user.userId'] = req.user;
        if (req.query.readStatus) findFilter['read'] = req.query.readStatus;

        // querying the db with the given parameters
        Notification.find(findFilter)
            .limit(size)
            .skip(size * (pageNo - 1))
            .sort(~createdAt)
            .select({ title: 1, description: 1, type: 1, redirectUrl: 1, createdAt: 1 })
            .exec((err, notifications) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });

                return res.status(200).send({ error: false, result: { notifications: notifications } });
            });
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

// function to mark the notification as read
middlewares.markReadNotification = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // checking for notification id parameter
        if (req.query.notificationId) {
            // making the query
            Notification.findById(req.query.notificationId, (err, notification) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });

                // if the user is the owner, change the read status
                if (notification.user.userId === req.user) {
                    notification.read = true;
                    // saving the notification object
                    notification.save((err) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        return res.status(200).send({ error: true, message: ['Notification marked read successfully'] });
                    });
                } else {
                    return res.status(401).send({ error: true, message: ['Not authorized to perform this operation'] });
                }
            });
        } else {
            return res.status(400).send({ error: true, message: ['Invalid url'] });
        }
    } else {
        return res.status(400).send({ error: true, message: errors.array() });
    }
};

// function to subscribe a device for notifications
middlewares.subscribe = async (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // checking the subscription with the userId exists or not
        if (await Subscription.exists({ userId: req.user })) {
            // updating the existing subscription
            Subscription.findOne({ userId: mongoose.Types.ObjectId(req.user) }).exec((err, subscription) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                if (!subscription) return res.status(400).send({ error: true, message: ['No such subscription exists'] });

                subscription.fcmToken = req.body.fcmToken;
                subscription.save((err, subscription) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    return res.status(200).send({
                        error: false,
                        result: {
                            message: 'subscription updated',
                            subscription: subscription,
                        },
                    });
                });
            });
        } else {
            // creating new subscription in case it doesnt exists already
            Subscription.create(
                {
                    userId: req.user,
                    userType: req.body.userType,
                    fcmToken: req.body.fcmToken,
                },
                (err, subscription) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    return res.status(201).send({ error: false, result: { message: 'subscription created', subscription: subscription } });
                }
            );
        }
    } else {
        return res.status(400).send({ error: true, message: [err.message] });
    }
};

// function to send the push notifications to the device
middlewares.sendNotification = (title, body, userId) => {
    // checking for the required parameters
    if (title && body && userId) {
        // find the subscription object for the given userId
        Subscription.findOne({ userId: userId }).exec((err, subscription) => {
            if (err) return [err.message, null];
            if (!subscription) return ['Invalid userId given', null];

            const notificationOptions = {
                priority: 'high',
                timeToLive: 60 * 60 * 24,
            };

            // sending the notification using fcm
            fcm.messaging()
                .sendToDevice(
                    subscription.fcmToken,
                    { notification: { title: title, body: body }, data: { testkey: 'testvalue' } },
                    notificationOptions
                )
                .then((response) => {
                    // create notification object here
                    return [null, { message: 'notification sent successfully', response: response }];
                })
                .catch((err) => {
                    console.log(err);
                    return [err.message, null];
                });
        });
    } else {
        return ['Required parameters are not there', null];
    }
};

middlewares.test = async (req, res, next) => {
    const response = await middlewares.sendNotification('test title', 'test body', req.user);
    return res.status(200).send(response);
};

module.exports = middlewares;
