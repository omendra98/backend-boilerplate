const Brand = require('../../models/brand');
const Campaign = require('../../models/campaign');
const Company = require('../../models/company');

const populateCategories = require('../util/populate-categories');

const fetchCampaignData = async (campaign, brand, company, isBookmarked) => {
    const campaignObject = {
        id: campaign._id,
        brandName: brand.name,
        logo: brand.logo,
        about: brand.about,
        companyName: company.name,
        createdOn: campaign.createdOn,
        lastEdittedOn: campaign.lastEdittedOn,
        activatedOn: campaign.activatedOn,
        status: campaign.status,
        brief: campaign.brief,
        criteria: campaign.criteria,
        budget: campaign.budget,
        startDate: campaign.startDate,
        endDate: campaign.endDate,
        isBookmarked: isBookmarked,
    };
    const categoryArray = await populateCategories(campaignObject.criteria.categories);

    for (let categoryIndex in categoryArray) campaignObject.criteria.categories[categoryIndex] = categoryArray[categoryIndex];
    return campaignObject;
};

const fetchCampaign = async (campaignId, isBookmarked) => {
    let isError = false;
    let isServerError = false;
    const result = await Campaign.findById(campaignId)
        .then((campaign) => {
            if (campaign != null) {
                return Brand.findById(campaign.brand)
                    .then((brand) => {
                        if (brand != null) {
                            return Company.findById(campaign.company)
                                .then((company) => {
                                    if (company != null) {
                                        return fetchCampaignData(campaign, brand, company, isBookmarked);
                                    } else {
                                        isError = true;
                                        isServerError = false;
                                        return ['No such company exists.'];
                                    }
                                })
                                .catch((error) => {
                                    isError = true;
                                    serverError = true;
                                    return [error.message];
                                });
                        } else {
                            isError = true;
                            isServerError = false;
                            return ['No such brand exists.'];
                        }
                    })
                    .catch((error) => {
                        isError = true;
                        serverError = true;
                        return [error.message];
                    });
            } else {
                isError = true;
                isServerError = false;
                return ['No such campaign exists.'];
            }
        })
        .catch((error) => {
            isError = true;
            serverError = true;
            return [error.message];
        });

    return { result: result, isError: isError, isServerError: isServerError };
};

module.exports = { fetchCampaign: fetchCampaign, fetchCampaignData: fetchCampaignData };
