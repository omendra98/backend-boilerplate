const { validationResult } = require('express-validator');

const mongoose = require('mongoose');

const Applicant = require('../../models/applicant');
const Campaign = require('../../models/campaign');
const Influencer = require('../../models/influencer');

const agenda = require('../../jobs/agenda');

const middlewares = {};

middlewares.applyCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty) {
        Applicant.findOne({
            influencer: req.user,
            campaign: req.body.campaignId,
        })
            .then((applicant) => {
                if (applicant == null) {
                    Campaign.findById(req.body.campaignId)
                        .then((campaign) => {
                            if (campaign != null) {
                                if (campaign.status === 'Active') {
                                    const applicant = new Applicant();
                                    applicant.influencer = req.user;
                                    applicant.campaign = req.body.campaignId;
                                    applicant.biddingAmount = req.body.biddingAmount;
                                    applicant.tags = JSON.parse(req.body.tags);
                                    applicant.caption = req.body.caption;
                                    for (let file of req.files) {
                                        applicant.moodboard.push(file.location);
                                    }
                                    applicant
                                        .save()
                                        .then((record) => {
                                            Influencer.findById(req.user)
                                                .then((influencer) => {
                                                    if (influencer != null) {
                                                        influencer.campaigns.push(req.body.campaignId);
                                                        influencer
                                                            .save()
                                                            .then((record) => {
                                                                campaign.applicantCount += 1;
                                                                campaign
                                                                    .save()
                                                                    .then(() => {
                                                                        res.status(200).json({
                                                                            error: false,
                                                                            message: ['Successfully applied to the campaign.'],
                                                                        });
                                                                    })
                                                                    .catch((error) => {
                                                                        res.status(500).json({ error: true, message: [error.message] });
                                                                    });
                                                            })
                                                            .catch((error) => {
                                                                res.status(500).json({ error: true, message: [error.message] });
                                                            });
                                                    } else {
                                                        res.status(400).json({ error: true, message: ['No such applicant exists.'] });
                                                    }
                                                })
                                                .catch((error) => {
                                                    res.status(500).json({ error: false, message: [error.message] });
                                                });
                                        })
                                        .catch((error) => {
                                            res.status(500).json({ error: false, message: [error.message] });
                                        });
                                } else {
                                    res.status(400).json({ error: true, message: ['You cannot apply to this campaign.'] });
                                }
                            } else {
                                res.status(400).json({ error: true, message: ['No such campaign exists.'] });
                            }
                        })
                        .catch((error) => {
                            res.status(500).json({ error: false, message: [error.message] });
                        });
                } else {
                    res.status(400).json({ error: true, message: ['Already applied to this campaign.'] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: false, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    }
};

// function to submit the link after application is approved
middlewares.updateLink = (req, res, next) => {
    // checking for validation errors
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // fetching the approved applicant object with the given details
        Applicant.findOne(
            { status: 'Approved', influencer: mongoose.Types.ObjectId(req.user), campaign: mongoose.Types.ObjectId(req.body.campaignId) },
            (err, applicant) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });
                if (!applicant)
                    return res
                        .status(404)
                        .send({ error: true, message: ['Cannot find any approved application with unverified link using given details'] });

                if (applicant.linkVerified) return res.status(400).send({ error: true, message: ['Link Already Verified'] });
                // updating the post link in the applicant's application
                if (req.body.postLink) {
                    applicant.link = req.body.postLink;
                    applicant.save((err) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        // setting the schedule to verify the link
                        agenda.schedule('5 seconds', 'verify link', { applicantId: applicant._id, counter: 3 });
                        console.log('verification scheduled');

                        return res.status(200).send({
                            error: false,
                            result: { message: 'The status of Post link verification will be updated after 2 days' },
                        });
                    });
                } else {
                    return res.status(400).send({ error: true, message: ['post Link is required'] });
                }
            }
        );
    } else {
        return res.status(400).send({ error: true, message: [errors.array()] });
    }
};

module.exports = middlewares;
