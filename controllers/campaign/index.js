const { validationResult } = require('express-validator');

const Company = require('../../models/company');
const Brand = require('../../models/brand');
const Campaign = require('../../models/campaign');
const Influencer = require('../../models/influencer');

const { fetchCampaign } = require('./fetch-campaign');

const agenda = require('../../jobs/agenda');

const middlewares = {};

// function to create a new campaign if not exist
// or to update the existing one
middlewares.upsertCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // getting user from jwt token
        Company.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not authorized to do this operation'] });

            // checking if brand id is provided or not
            if (req.body.brandId) {
                // checking if brand is there in company's list
                if (user.brands.includes(req.body.brandId)) {
                    Brand.findById(req.body.brandId, (err, brand) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        if (req.body.campaignId) {
                            // logic when campaign exists
                            if (brand.campaigns.includes(req.body.campaignId)) {
                                // fetching the campaign object
                                Campaign.findById(req.body.campaignId, {}, async (err, campaign) => {
                                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                                    if (campaign.status === 'Active')
                                        return res
                                            .status(400)
                                            .send({ error: true, message: ['Cannot make changes to this campaign as it is activated.'] });

                                    // update the info of the campaign here
                                    campaign.lastEdittedOn = new Date();
                                    if (req.body.brief) {
                                        if (req.body.brief.objective) campaign.brief.objective = req.body.brief.objective;
                                        if (req.body.brief.title) campaign.brief.title = req.body.brief.title;
                                        if (req.body.brief.brief) campaign.brief.brief = req.body.brief.brief;
                                        if (req.body.brief.type) campaign.brief.type = req.body.brief.type;
                                        if (req.body.brief.pageUrl) campaign.brief.pageUrl = req.body.brief.pageUrl;
                                        if (req.body.brief.coverPhoto.url) campaign.brief.coverPhoto.url = req.body.brief.coverPhoto.url;
                                        if (req.body.brief.caption) campaign.brief.caption = req.body.brief.caption;
                                        if (req.body.brief.product) {
                                            if (req.body.brief.product.providedBy)
                                                campaign.brief.product.providedBy = req.body.brief.product.providedBy;
                                            if (req.body.brief.product.optionalDetails)
                                                campaign.brief.product.optionalDetails = req.body.brief.product.optionalDetails;
                                        }
                                    }
                                    if (req.body.criteria) {
                                        if (req.body.criteria.gender) campaign.criteria.gender = req.body.criteria.gender;
                                        if (req.body.criteria.city) campaign.criteria.city = req.body.criteria.city;
                                        if (req.body.criteria.age) {
                                            if (req.body.criteria.age.min) campaign.criteria.age.min = req.body.criteria.age.min;
                                            if (req.body.criteria.age.max) campaign.criteria.age.max = req.body.criteria.age.max;
                                        }
                                        if (req.body.criteria.followersCount) {
                                            if (req.body.criteria.followersCount.min)
                                                campaign.criteria.followersCount.min = req.body.criteria.followersCount.min;
                                            if (req.body.criteria.followersCount.max)
                                                campaign.criteria.followersCount.max = req.body.criteria.followersCount.max;
                                        }

                                        if (req.body.criteria.categories) campaign.criteria.categories = req.body.criteria.categories;
                                        if (req.body.criteria.moodboard) campaign.criteria.moodboard = req.body.criteria.moodboard;
                                        if (req.body.criteria.description) campaign.criteria.description = req.body.criteria.description;
                                        if (req.body.criteria.dos) campaign.criteria.dos = req.body.criteria.dos;
                                        if (req.body.criteria.donts) campaign.criteria.donts = req.body.criteria.donts;
                                        if (req.body.criteria.hashtags) campaign.criteria.hashtags = req.body.criteria.hashtags;
                                        if (req.body.criteria.house) campaign.criteria.house = req.body.criteria.house;
                                    }
                                    if (req.body.budget) campaign.budget = req.body.budget;
                                    if (req.body.startDate) campaign.startDate = req.body.startDate;
                                    if (req.body.endDate) {
                                        campaign.endDate = req.body.endDate;
                                        const jobs = await agenda.jobs({ name: 'expire campaign' });
                                        for (const job of jobs) {
                                            if (job.attrs.data.campaignId === campaign._id) {
                                                try {
                                                    await job.remove();
                                                    console.log('Successfully removed job from collection');
                                                } catch (e) {
                                                    console.error('Error removing job from collection');
                                                }
                                            }
                                        }
                                        agenda.schedule(campaign.endDate, 'expire campaign', { campaignId: campaign._id });
                                    }

                                    // save the campaign
                                    campaign.save((err, campaign) => {
                                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                                        return res.status(200).send({ error: false, result: { campaign: campaign } });
                                    });
                                });
                            } else {
                                return res.status(400).send({ error: true, message: ['Invalid campaign Id'] });
                            }
                        } else {
                            // logic when campaign doesn't exists
                            Campaign.create(
                                {
                                    brand: brand._id,
                                    company: user._id,
                                    createdOn: new Date(),
                                    lastEdittedOn: new Date(),
                                    brief: {
                                        objective: req.body.brief && req.body.brief.objective ? req.body.brief.objective : '',
                                        title: req.body.brief && req.body.brief.title ? req.body.brief.title : '',
                                        brief: req.body.brief && req.body.brief.brief ? req.body.brief.brief : '',
                                        type: req.body.brief && req.body.brief.type ? req.body.brief.type : '',
                                        pageUrl: req.body.brief && req.body.brief.pageUrl ? req.body.brief.pageUrl : '',
                                        caption: req.body.brief && req.body.brief.caption ? req.body.brief.caption : '',
                                        coverPhoto: {
                                            url:
                                                req.body.brief && req.body.brief.coverPhoto && req.body.brief.coverPhoto.url
                                                    ? req.body.brief.coverPhoto.url
                                                    : '',
                                        },
                                        product: {
                                            providedBy:
                                                req.body.brief && req.body.brief.product && req.body.brief.product.providedBy
                                                    ? req.body.brief.product.providedBy
                                                    : '',
                                            optionalDetails:
                                                req.body.brief && req.body.brief.product && req.body.brief.product.optionalDetails
                                                    ? req.body.brief.product.optionalDetails
                                                    : '',
                                        },
                                    },
                                    criteria: {
                                        gender: req.body.criteria && req.body.criteria.gender ? req.body.criteria.gender : '',
                                        city: req.body.criteria && req.body.criteria.city ? req.body.criteria.city : '',
                                        age: {
                                            min:
                                                req.body.criteria && req.body.criteria.age && req.body.criteria.age.min
                                                    ? req.body.criteria.age.min
                                                    : '',
                                            max:
                                                req.body.criteria && req.body.criteria.age && req.body.criteria.age.max
                                                    ? req.body.criteria.age.max
                                                    : '',
                                        },
                                        followersCount: {
                                            min:
                                                req.body.criteria && req.body.criteria.followersCount.min
                                                    ? req.body.criteria.followersCount.min
                                                    : '',
                                            max:
                                                req.body.criteria && req.body.criteria.followersCount.max
                                                    ? req.body.criteria.followersCount.max
                                                    : '',
                                        },
                                        categories: req.body.criteria && req.body.criteria.categories ? req.body.criteria.categories : [],
                                        moodboard: req.body.criteria && req.body.criteria.moodboard ? req.body.criteria.moodboard : [],
                                        description:
                                            req.body.criteria && req.body.criteria.description ? req.body.criteria.description : '',
                                        dos: req.body.criteria && req.body.criteria.dos ? req.body.criteria.dos : [],
                                        donts: req.body.criteria && req.body.criteria.donts ? req.body.criteria.donts : [],
                                        hashtags: req.body.criteria && req.body.criteria.hashtags ? req.body.criteria.hashtags : '',
                                        donts: req.body.criteria && req.body.criteria.house ? req.body.criteria.house : [],
                                    },
                                    budget: req.body.budget ? req.body.budget : '',
                                    startDate: req.body.startDate ? req.body.startDate : '',
                                    endDate: req.body.endDate ? req.body.endDate : '',
                                },
                                async (err, campaign) => {
                                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                                    const jobs = await agenda.jobs({ name: 'expire campaign' });
                                    for (const job of jobs) {
                                        if (job.attrs.data.campaignId === campaign._id) {
                                            try {
                                                await job.remove();
                                                console.log('Successfully removed job from collection');
                                            } catch (e) {
                                                console.error('Error removing job from collection');
                                            }
                                        }
                                    }
                                    agenda.schedule(campaign.endDate, 'expire campaign', { campaignId: campaign._id });
                                    brand.campaigns.push(campaign._id);
                                    brand.save();
                                    return res.status(201).send({ error: false, result: { campaign: campaign } });
                                }
                            );
                        }
                    });
                } else {
                    return res.status(400).send({ error: true, message: ['You are not authorized to do this operation'] });
                }
            } else {
                return res.status(500).send({ error: true, message: ['Brand Id is not provided'] });
            }
        });
    }
};

// function to get all the campaigns of the user
middlewares.getCampaigns = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Company.findById(req.user).then(async (user) => {
            if (user != null) {
                let campaignsList = [];
                for (const brandId of user.brands) {
                    await Brand.findById(brandId)
                        .then(async (brand) => {
                            if (brand != null) {
                                for (const campaignId of brand.campaigns) {
                                    await Campaign.findById(campaignId)
                                        .then((campaign) => {
                                            if (campaign != null) {
                                                campaignsList.push(campaign);
                                            }
                                        })
                                        .catch((err) => {
                                            console.log(err);
                                        });
                                }
                            }
                        })
                        .catch((err) => {
                            console.log(err);
                        });
                }

                return res.status(200).send(campaignsList);
            } else {
                return res.status(400).send({ error: true, message: ['Not authorized to do this operation'] });
            }
        });
    }
};

// function to get a specific campaign
middlewares.getCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Company.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not authorized to perform this operation'] });

            Campaign.findById(req.params.id, (err, campaign) => {
                if (err) return res.status(500).send({ error: true, message: ['Invalid campaign Id'] });

                return res.status(200).send({ error: false, result: { campaign: campaign } });
            });
        });
    }
};

// functions to get all the campaigns of a specific brand of the user
middlewares.getCampaignsByBrandId = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Brand.findById(req.params.id)
            .then(async (brand) => {
                if (brand != null) {
                    let campaigns = [];
                    for (const campaignId of brand.campaigns) {
                        await Campaign.findById(campaignId)
                            .then((campaign) => {
                                if (campaign != null) {
                                    campaigns.push(campaign);
                                }
                            })
                            .catch((error) => {
                                console.log(error);
                            });
                    }
                    res.status(200).json({ error: false, result: campaigns });
                } else {
                    res.status(400).json({ error: true, message: ['No such brand exists.'] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: ['validation error in user fields.'] });
    }
};

// function to delete a campaign
middlewares.deleteCampaign = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Company.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not authorized to perform this operation'] });

            // checking if the user is the owner of the brand
            if (user.brands.includes(req.body.brandId)) {
                Brand.findById(req.body.brandId, (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });
                    if (!brand) return res.status(400).send({ error: true, message: ['No such brand exists'] });

                    // checking if the campaign is of specified brand
                    if (brand.campaigns.includes(req.body.campaignId)) {
                        Campaign.findByIdAndDelete(req.body.campaignId, (err) => {
                            if (err) return res.status(500).send({ error: true, message: [err.message] });

                            // deleting the campaign
                            const campaignIndex = brand.campaigns.indexOf(req.body.campaignId);
                            brand.campaigns.splice(campaignIndex, 1);
                            brand.save((err, savedBrand) => {
                                if (!err) return res.status(200).send({ error: false, message: ['Campaign deleted successfully'] });
                            });
                        });
                    } else {
                        return res.status(400).send({ error: true, message: ['This campaign is not accessible'] });
                    }
                });
            } else {
                return res.status(400).send({ error: true, message: ['This brandId is not accessible'] });
            }
        });
    }
};

// function to get all the campaign according to their status
middlewares.getCampaignsByStatus = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        // checking if the user is valid or not
        Company.findById(req.user, async (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not authorized to perform this operation'] });

            let filteredCampaigns = [];

            // finding all the brands of the user
            for (const brandId of user.brands) {
                await Brand.findById(brandId)
                    .then(async (brand) => {
                        // getting all the campaigns and checking the status of each campaign
                        for (const campaignId of brand.campaigns) {
                            await Campaign.findById(campaignId)
                                .then((campaign) => {
                                    // if the status is All then push all of them in the list
                                    if (req.params.status === 'All') {
                                        filteredCampaigns.push(campaign);
                                    } else {
                                        //  else check which campaigns to push
                                        if (campaign.status === req.params.status) {
                                            filteredCampaigns.push(campaign);
                                        }
                                    }
                                })
                                .catch((err) => console.log(err));
                        }
                    })
                    .catch((err) => console.log(err));
            }
            return res.status(200).send({ error: false, result: { campaigns: filteredCampaigns } });
        }).catch((err) => console.log(err));
    }
};

module.exports = middlewares;
