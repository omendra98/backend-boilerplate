const Brand = require('../../models/brand');
const Company = require('../../models/company');

const fetchBrandData = async (brand, company, isBookmarked) => {
    const brandObject = {
        id: brand._id,
        name: brand.name,
        logo: brand.logo,
        about: brand.about,
        companyName: company.name,
        isBookmarked: isBookmarked,
    };

    return brandObject;
};

const fetchBrand = async (brandId, isBookmarked) => {
    let isError = false;
    let isServerError = false;
    const result = await Brand.findById(brandId)
        .then((brand) => {
            if (brand != null) {
                return Company.findById(brand.companyId)
                    .then((company) => {
                        if (company != null) {
                            return fetchBrandData(brand, company, isBookmarked);
                        } else {
                            isError = true;
                            isServerError = false;
                            return ['No such company exists.'];
                        }
                    })
                    .catch((error) => {
                        isError = true;
                        serverError = true;
                        return [error.message];
                    });
            } else {
                isError = true;
                isServerError = false;
                return ['No such brand exists.'];
            }
        })
        .catch((error) => {
            isError = true;
            serverError = true;
            return [error.message];
        });

    return { result: result, isError: isError, isServerError: isServerError };
};

module.exports = { fetchBrand: fetchBrand, fetchBrandData: fetchBrandData };
