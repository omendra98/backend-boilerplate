const { validationResult } = require('express-validator');

const Company = require('../../models/company');
const Brand = require('../../models/brand');

const brandMiddlewares = {};

// function to create a new brand
brandMiddlewares.createBrand = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // searching for a company using the JWT
        Company.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not a valid user'] });

            // creating new brand
            Brand.create(
                {
                    companyId: req.user,
                    name: req.body.name,
                    coverUrl: req.body.coverUrl,
                    logo: {
                        url: req.body.logo,
                    },
                    about: req.body.about,
                },
                (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    brand.save();
                    user.brands.push(brand._id);
                    user.save();
                    return res.status(200).send({ error: false, result: { brand: brand } });
                }
            );
        });
    }
};

// function to create a new brand
brandMiddlewares.editBrand = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // searching for a company using the JWT
        Company.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not a valid user'] });

            // updating if the correct brand id
            if (user.brands.includes(req.body.id)) {
                Brand.findById(req.body.id, { name: 1, logo: 1, about: 1, campaigns: 1, coverUrl: 1, rating: 1 }, (err, brand) => {
                    if (err) return res.status(500).send({ error: true, message: [err.message] });

                    // checking which values are there
                    if (req.body.name) {
                        brand.name = req.body.name;
                    } // checking the name parameter
                    if (req.body.logo) {
                        brand.logo.url = req.body.logo;
                    } // checking the logo parameter
                    if (req.body.about) {
                        brand.about = req.body.about;
                    } // checking the about parameter
                    if (req.body.coverUrl) {
                        brand.coverUrl = req.body.coverUrl;
                    } // checking the coverUrl parameter
                    // updating the brand
                    brand.save((err, brand) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        return res.status(200).send({ error: true, result: { brand: brand } });
                    });
                });
            } else {
                return res.status(400).send({ error: true, message: ['Not authorized to edit this brand.'] });
            }
        });
    }
};

// function to fetch all the brands of a company
brandMiddlewares.viewBrands = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // getting the company to get all its brands
        Company.findById(req.user, async (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not a valid user'] });

            // getting details of all brands
            output = [];
            for (const brand of user.brands) {
                await Brand.findById(
                    brand,
                    { id: 1, name: 1, logo: 1, about: 1, campaigns: 1, companyId: 1, coverUrl: 1, rating: 1 },
                    (err, item) => {
                        if (err) console.log(err);
                        output.push(item);
                    }
                );
            }
            return res.status(200).send({ error: false, result: { brands: output } });
        });
    }
};

// function to fetch a specific brand using brand Id
brandMiddlewares.viewBrand = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // checking if the user is the owner of the request brand
        Company.findById(req.user, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(400).send({ error: true, message: ['Not authorized to perform this operation'] });

            // checking if the brand exists and returns if it exists
            if (req.params.id && user.brands.includes(req.params.id)) {
                Brand.findById(
                    req.params.id,
                    { _id: 1, logo: 1, campaigns: 1, name: 1, about: 1, companyId: 1, coverUrl: 1, rating: 1 },
                    (err, brand) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });
                        if (!brand) return res.status(400).send({ error: true, message: ['No such brand exists'] });

                        return res.status(200).send({ error: false, result: { brand: brand } });
                    }
                );
            } else {
                return res.status(400).send({ error: true, message: ['Not authorized to access this brand'] });
            }
        });
    }
};

module.exports = brandMiddlewares;
