const { validationResult } = require('express-validator');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var crypto = require('crypto');

var config = require('../../config');

var User = require('../../models/company');

var ses = require('../../aws/aws-ses');

const middlewares = {};

// function to create a new user
middlewares.createUser = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        User.findOne({ email: req.body.email }, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: ['something went wrong'] });
            if (user) {
                if (!user.verificationStatus)
                    return res.status(401).send({
                        error: true,
                        message: ['The user exists but not verified. Verification link is sent to registered email address.'],
                    });
                return res.status(401).send({ error: true, message: ['User with this email already exists'] });
            }

            // hasing the password for security purposes
            var hashedPassword = bcrypt.hashSync(req.body.password);

            // creating instance of user collection
            User.create(
                {
                    email: req.body.email,
                    password: hashedPassword,
                    name: req.body.name,
                    mobile: req.body.mobile,
                    accountHolderName: req.body.accountHolderName,
                },
                (err, user) => {
                    // check if there is any error
                    if (err) return res.status(500).send({ error: true, message: ['There was a problem registering the user.'] });

                    // Generate and set password reset token
                    user.verificationToken = crypto.randomBytes(20).toString('hex');
                    user.verificationStatus = false;
                    user.save((err) => {
                        if (err) return res.status(500).send({ error: true, message: [err.message] });

                        // Generating message that is to be sent in email
                        let link = `http://${req.headers.host}/company/register/verify/${user.verificationToken}`;
                        let message = `Hi ${user.accountHolderName} \n
                        Please click on the following link ${link} to verify your email. \n\n`;

                        let subject = 'Account Verification Email';

                        // sending email using ses service
                        ses.sendEmail(user.email, subject, message);
                        return res
                            .status(200)
                            .send({ error: false, result: `A verification mail has been sent to the user at ${user.email}` });
                    });
                }
            );
        });
    }
};

// helper function to verify the user
middlewares.verifyUser = (req, res) => {
    User.findOne({ verificationToken: req.params.token, verificationStatus: false }, (err, user) => {
        if (err) return res.status(500).send({ error: true, message: [err.message] });
        if (!user) return res.status(401).send({ error: true, message: ['Token expired or user already verified.'] });

        // updating the verification status of the user
        user.verificationStatus = true;
        user.verificationToken = undefined;
        user.save((err) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });

            // send the response
            res.status(200).render('verified');
        });
    });
};

// function to login a user
middlewares.login = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        // searching for a user instance and error handling
        User.findOne({ email: req.body.email }, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: ['Error on the server'] });
            if (!user) return res.status(404).send({ error: true, message: ['No User Found.'] });
            if (!user.verificationStatus) return res.status(401).send({ error: true, message: ['User is not verified. Verify to login.'] });

            // comparing the password by hashing and comparing
            var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
            if (!passwordIsValid) return res.status(401).send({ error: true, message: ['Invalid credentials'] });

            // generating token for the user
            var token = jwt.sign(
                {
                    userId: user._id,
                    userType: 'company',
                },
                config.JWT_SECRET,
                {
                    expiresIn: 86400, //expires in 24 hours
                }
            );

            // sending the response
            res.status(200).send({ error: false, result: { auth: true, token: token } });
        });
    }
};

module.exports = middlewares;
