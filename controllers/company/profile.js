const { validationResult } = require('express-validator');

const Company = require('../../models/company');

const profileMiddlewares = {};

// function to get the profile details of the company
profileMiddlewares.getProfile = (req, res, next) => {
    // get the company object using the jwt token from headers
    Company.findById(
        req.user,
        {
            _id: 1,
            email: 1,
            name: 1,
            accountHolderName: 1,
            mobile: 1,
            verificationStatus: 1,
            brands: 1,
            address: 1,
        },
        (err, user) => {
            if (err) return res.status(500).send({ error: true, message: ['There is a problem finding the user.'] });
            if (!user) return res.status(404).send({ error: true, message: ['No user found'] });

            res.status(200).send({ error: false, result: user });
        }
    );
};

// function to edit the profile details of the company
profileMiddlewares.editProfile = (req, res, next) => {
    // finding the company using jwt token
    Company.findById(
        req.user,
        {
            _id: 1,
            email: 1,
            name: 1,
            accountHolderName: 1,
            mobile: 1,
            verificationStatus: 1,
            brands: 1,
            address: 1,
        },
        (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });

            // checking each parameter if it is available,
            // if available update it.

            if (req.body.mobile) {
                user.mobile = req.body.mobile;
            }
            if (req.body.accountHolderName) {
                user.accountHolderName = req.body.accountHolderName;
            }
            if (req.body.address) {
                if (req.body.address.base) user.address.address.base = req.body.address.base;
                if (req.body.address.city) user.address.address.city = req.body.address.city;
                if (req.body.address.state) user.address.address.state = req.body.address.state;
                if (req.body.address.GSTN) user.address.address.GSTN = req.body.address.GSTN;
                if (req.body.address.pincode) user.address.address.pincode = req.body.address.pincode;
                if (
                    user.address.address.base !== '' &&
                    user.address.address.city !== '' &&
                    user.address.address.state !== '' &&
                    user.address.address.GSTN !== '' &&
                    user.address.address.pincode !== 0
                )
                    user.address.available = true;
            }

            user.save((err) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });

                return res.status(200).send({ error: false, result: user });
            });
        }
    );
};

module.exports = profileMiddlewares;
