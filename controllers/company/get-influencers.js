const { validationResult } = require('express-validator');
const Influencer = require('../../models/influencer');
const populateCategories = require('../util/populate-categories');

const middlewares = {};

middlewares.getInfluencer = (req, res, next) => {
    const errors = validationResult(req);
    if (errors.isEmpty()) {
        Influencer.findById(req.query.influencer)
            .then(async (influencer) => {
                if (influencer != null) {
                    const populatedCategories = await populateCategories(influencer.categories);
                    for (let categoryIndex in populatedCategories) {
                        influencer.categories[categoryIndex] = populatedCategories[categoryIndex];
                    }
                    res.status(200).json({ error: false, result: influencer });
                } else {
                    res.status(400).json({ error: true, message: ['Influencer does not exists.'] });
                }
            })
            .catch((error) => {
                res.status(500).json({ error: true, message: [error.message] });
            });
    } else {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    }
};

middlewares.getInfluencers = (req, res, next) => {
    const pageNo = req.query.pageNo ? parseInt(req.query.pageNo) : 1;
    const size = req.query.size ? parseInt(req.query.size) : 20;

    const queryFilter = {};

    if (req.query.categories) {
        queryFilter.categories = { $all: req.query.categories };
    }

    if (req.query.location !== '') {
        queryFilter.city = req.query.location;
    }

    if (req.query.start != -1 && req.query.end != -1) {
        queryFilter['platforms.instagram.numberOfFollowers'] = { $gte: Number(req.query.start), $lte: Number(req.query.end) };
    }

    if (pageNo <= 0) return res.status(400).send({ error: true, message: ['Invalid page number, should start with 1'] });

    Influencer.find(queryFilter)
        .limit(size)
        .skip(size * (pageNo - 1))
        .sort(req.query.sortBy)
        .then(async (influencers) => {
            for (let index in influencers) {
                const populatedCategories = await populateCategories(influencers[index].categories);
                for (let categoryIndex in populatedCategories) {
                    influencers[index].categories[categoryIndex] = populatedCategories[categoryIndex];
                }
            }
            return res.status(200).send({ error: false, result: influencers });
        })
        .catch((error) => {
            return res.status(400).send({ error: true, message: [error.message] });
        });
};

module.exports = middlewares;
