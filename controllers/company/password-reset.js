const { validationResult } = require('express-validator');
var bcrypt = require('bcryptjs');
var crypto = require('crypto');

var User = require('../../models/company');

var ses = require('../../aws/aws-ses');

const middlewares = {};

// Recover password - Generates token and sends password reset email.
middlewares.recover = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        User.findOne({ email: req.body.email }, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user)
                return res.status(401).send({
                    error: true,
                    message: [
                        `The email address ${req.body.email} is not associated with any account. Double check your email address and try again.`,
                    ],
                });

            // Generate and set password reset token
            user.resetPasswordToken = crypto.randomBytes(20).toString('hex');
            user.resetPasswordExpires = Date.now() + 3600000; // expires in an hour

            // save the updated user object
            user.save((err) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });

                // Generating message that is to be sent in email
                let link = `http://${req.headers.host}/company/reset/${user.resetPasswordToken}`;
                let message = `Hi ${user.accountHolderName} \n
                Please click on the following link ${link} to reset your password. \n\n
                If you did not request this, please ignore this email and your password will remain unchanged.\n`;

                let subject = 'Password Change Request';

                // sending email using ses service
                ses.sendEmail(user.email, subject, message);
                return res.status(200).send({ error: false, result: `A reset mail has been sent to the user at ${user.email}` });
            });
        });
    }
};

// Reset Password - Validate password reset token and shows the password reeset view
middlewares.reset = (req, res) => {
    User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, (err, user) => {
        if (err) return res.status(500).send({ error: true, message: [err.message] });
        if (!user) return res.status(401).send({ error: true, message: ['Password reset token is invalid or has expired.'] });

        // Redirect user to form with the email address
        res.status(200).render('reset');
    });
};

// Reset Password
middlewares.resetPassword = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json({ error: true, message: ['Validation errors in user fields.'] });
    } else {
        User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, (err, user) => {
            if (err) return res.status(500).send({ error: true, message: [err.message] });
            if (!user) return res.status(401).send({ error: true, message: ['Password reset token is invalid or has expired.'] });

            // hasing the password for security purposes
            var hashedPassword = bcrypt.hashSync(req.body.password);

            // Set the new password
            user.password = hashedPassword;
            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;

            // Save
            user.save((err) => {
                if (err) return res.status(500).send({ error: true, message: [err.message] });

                // Send Email
                let message = `Hi ${user.fullName} \n
                This is a confirmation that the password for your account ${user.email} has just been changed.\n`;

                let subject = 'Password Change Request';

                ses.sendEmail(user.email, subject, message);
                return res.status(200).send({ error: false, result: 'Password changed successfully.' });
            });
        });
    }
};

module.exports = middlewares;
