const axios = require('axios');

const Applicant = require('../../models/applicant');
const Transaction = require('../../models/transaction');
const Influencer = require('../../models/influencer');
const Wallet = require('../../models/wallet');

module.exports = (agenda) => {
    // defining the verify link agenda
    agenda.define('verify link', async (job, done) => {
        // fetching the applicant object for the agenda
        await Applicant.findById(job.attrs.data.applicantId).exec(async (err, applicant) => {
            // checking for system error
            if (err) {
                console.log('system error');
                console.log(err.message);
                done();
                return;
            }
            // checking if the applicant id is valid or not
            if (!applicant) {
                console.log('No such application');
                done();
                return;
            }
            // if it is final verification
            if (job.attrs.data.counter === 0) {
                // update the link status
                applicant.linkVerified = true;
                applicant.status = 'Submitted';
                applicant.save(async (err) => {
                    if (!err) {
                        console.log("Applicant's link verified successfully");

                        // transaction to be added to user account
                        await Transaction.create(
                            {
                                influencer: applicant.influencer,
                                type: 'credit',
                                amount: applicant.biddingAmount,
                                entity: {
                                    entityType: 'Campaign',
                                    entityId: applicant.campaign,
                                },
                            },
                            async (err, transaction) => {
                                if (err) {
                                    console.log(err.message);
                                    done();
                                    return;
                                } else {
                                    // fetching the influencer for wallet Id
                                    await Influencer.findById(applicant.influencer, async (err, influencer) => {
                                        if (err) {
                                            console.log(err.message);
                                            done();
                                            return;
                                        }

                                        if (!influencer) {
                                            console.log('no such influencer exists');
                                            done();
                                            return;
                                        }

                                        // fetching the wallet using the walledId
                                        // to update the amount in wallet
                                        await Wallet.findById(influencer.walletId).exec((err, wallet) => {
                                            if (err) {
                                                console.log(err.message);
                                                done();
                                                return;
                                            }

                                            if (!wallet) {
                                                console.log('No wallet exists for this user');
                                                done();
                                                return;
                                            }

                                            wallet.totalEarnings += transaction.amount;
                                            wallet.moneyToBeWithdrawn += transaction.amount;
                                            wallet.save((err) => {
                                                if (err) {
                                                    console.log(err.message);
                                                    done();
                                                    return;
                                                }

                                                // agenda.schedule('24 hours', 'release payout', {
                                                //     influencerId: influencer._id,
                                                // });

                                                console.log("Payment credited to influencer's account successfully");
                                                done();
                                                return;
                                            });
                                        });
                                    });
                                }
                            }
                        );

                        done();
                    } else {
                        console.log(err.message);
                        done();
                        return;
                    }
                });
            } else {
                // validating the provided link
                await axios
                    .get(applicant.link)
                    .then((res) => {
                        if (res.status === 200) {
                            // if the response is 200
                            // schedule the next verification
                            agenda.schedule('24 hours', 'verify link', {
                                applicantId: job.attrs.data.applicantId,
                                counter: job.attrs.data.counter - 1,
                            });
                            done();
                            return;
                        } else {
                            console.log('Invalid link provided');
                            done();
                            return;
                        }
                    })
                    .catch((err) => {
                        console.log(err.message);
                        done();
                        return;
                    });
            }
        });
    });
};
