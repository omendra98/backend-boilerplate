const Campaign = require('../../models/campaign');

module.exports = (agenda) => {
    agenda.define('expire campaign', async (job, done) => {
        await Campaign.findById(job.attrs.data.campaignId).then((campaign) => {
            if (campaign != null) {
                campaign.status = 'Expired';
                campaign
                    .save()
                    .then((record) => {
                        console.log('Campaign is now expired.');
                        done();
                        return;
                    })
                    .catch((error) => {
                        console.log(error);
                        done();
                        return;
                    });
            } else {
                console.log('No such campaign exists.');
                done();
                return;
            }
        });
    });
};
