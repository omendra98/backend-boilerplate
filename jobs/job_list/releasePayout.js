const Influencer = require('../../models/influencer');
const Wallet = require('../../models/wallet');
const payouts = require('../../controllers/admin/payouts');

module.exports = (agenda) => {
    agenda.define('release payout', async (job, done) => {
        await Influencer.findById(job.attrs.data.influencerId).then((influencer) => {
            if (influencer != null) {
                Wallet.findById(influencer.walletId).then((wallet) => {
                    if (wallet != null) {
                        payouts(
                            'account_number',
                            influencer.defaultAccount,
                            wallet.moneyToBeWithdrawn * 100,
                            'INR',
                            'IMPS',
                            'Influencer Payouts',
                            true,
                            'Influencer Campaign Post Payment',
                            {}
                        )
                            .then((response) => {
                                wallet.moneyToBeWithdrawn = 0;
                                wallet
                                    .save((record) => {
                                        console.log('Payout is being made.');
                                        done();
                                        return;
                                    })
                                    .catch((error) => {
                                        console.log(error);
                                        done();
                                        return;
                                    });
                            })
                            .catch((error) => {
                                console.log(error);
                                done();
                                return;
                            });
                    } else {
                        console.log('No such influencer wallet exists.');
                        done();
                        return;
                    }
                });
            } else {
                console.log('No such influencer exists.');
                done();
                return;
            }
        });
    });
};
