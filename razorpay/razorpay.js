const Razorpay = require('razorpay');
const axios = require('axios');

const config = require('../config');

// configuring the razorpay credentials
const razorpayInstance = new Razorpay({
    key_id: config.RAZORPAY_KEY_ID,
    key_secret: config.RAZORPAY_KEY_SECRET,
});

const axiosInstance = axios.create({
    baseURL: 'https://api.razorpay.com/v1',
    auth: {
        username: config.RAZORPAY_KEY_ID,
        password: config.RAZORPAY_KEY_SECRET,
    },
});

module.exports = { razorpayInstance, axiosInstance };
