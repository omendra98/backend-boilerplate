const mongoose = require('mongoose');
require('mongoose-double')(mongoose);

const SchemaTypes = mongoose.Schema.Types;

const InfluencerSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            default: '',
        },
        dob: {
            type: Date,
            default: null,
        },
        gender: {
            type: String,
            default: '',
        },
        city: {
            type: String,
            default: '',
        },
        country: {
            type: String,
            default: '',
        },
        email: {
            type: String,
            default: '',
        },
        categories: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'categories',
            },
        ],
        bookmarkedCampaigns: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'campaigns',
            },
        ],
        campaigns: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'campaigns',
            },
        ],
        phoneNumber: {
            type: String,
            default: '',
        },
        otpToken: Number,
        otpExpiry: Date,
        platforms: {
            instagram: {
                type: {
                    userId: String,
                    username: String,
                    accessToken: String,
                    accessTokenExpiry: Date,
                    numberOfFollowers: Number,
                    numberOfFollowing: Number,
                    fullName: String,
                    profilePicture: String,
                    profilePictureHD: String,
                    engagement: SchemaTypes.Double,
                },
                default: null,
            },
        },
        fundsAccounts: {
            type: Array,
            default: [],
        },
        defaultAccount: {
            type: Number,
            default: -1,
        },
        walletId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'wallets',
        },
        newsletter: {
            type: Boolean,
            default: false,
        },
        referee: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
            default: null,
        },
        credits: {
            type: Number,
            default: 0,
        },
    },
    { timestamps: true }
);

const Influencer = mongoose.model('influencer', InfluencerSchema);

module.exports = Influencer;
