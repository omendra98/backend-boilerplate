const mongoose = require('mongoose');

const LibrarySchema = new mongoose.Schema(
    {
        influencerId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
        },
        url: {
            type: String,
            default: '',
            required: [true, 'URL is required'],
        },
        price: {
            type: Number,
            default: 0,
            required: [true, 'Price is required'],
        },
        caption: {
            type: String,
            default: '',
        },
        bookmarkedBy: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'companies',
            },
        ],
        paidPartnershipAllowed: {
            type: Boolean,
            default: false,
        },
        paid: {
            type: Boolean,
            default: false,
        },
        paidBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companies',
            default: null,
        },
        tag: {
            brandId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'brands',
                default: null,
            },
            brandName: {
                type: String,
                default: '',
            },
            companyId: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'companies',
                default: null,
            },
        },
    },
    { timestamps: true }
);

const Library = mongoose.model('library', LibrarySchema);

module.exports = Library;
