const mongoose = require('mongoose');

const CompanySchema = new mongoose.Schema(
    {
        email: {
            type: String,
            required: [true, 'Email is required'],
        },
        password: {
            type: String,
            required: [true, 'Password is required'],
        },
        name: {
            type: String,
            required: [true, 'Company name is required'],
        },
        mobile: {
            type: String,
            required: [true, 'Contact No. is required'],
        },
        accountHolderName: {
            type: String,
            required: [true, "Account Holder's name is required"],
        },
        address: {
            available: {
                type: Boolean,
                default: false,
            },
            address: {
                base: {
                    type: String,
                    default: '',
                },
                city: {
                    type: String,
                    default: '',
                },
                state: {
                    type: String,
                    default: '',
                },
                GSTN: {
                    type: String,
                    default: '',
                },
                pincode: {
                    type: Number,
                    default: 0,
                },
            },
        },
        verificationStatus: {
            type: Boolean,
            default: false,
            required: true,
        },
        verificationToken: {
            type: String,
            required: false,
        },
        resetPasswordToken: {
            type: String,
            required: false,
        },
        resetPasswordExpires: {
            type: Date,
            required: false,
        },
        brands: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'brands',
            },
        ],
        favInfluencers: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'influencers',
            },
        ],
    },
    { timestamps: true }
);

const Company = mongoose.model('company', CompanySchema);

module.exports = Company;
