const mongoose = require('mongoose');

const ApplicantSchema = new mongoose.Schema(
    {
        status: {
            type: String,
            default: 'Applied',
        },
        influencer: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'influencers',
        },
        campaign: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'campaigns',
        },
        biddingAmount: {
            type: Number,
        },
        tags: {
            instagram: {
                type: Array,
                default: [],
            },
        },
        moodboard: {
            type: Array,
            default: [],
        },
        caption: {
            type: String,
            default: '',
        },
        link: {
            type: String,
            default: '',
        },
        linkVerified: {
            type: Boolean,
            default: false,
        },
    },
    { timestamps: true }
);

const Applicant = mongoose.model('applicant', ApplicantSchema);

module.exports = Applicant;
