const mongoose = require('mongoose');

const BrandSchema = new mongoose.Schema(
    {
        companyId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'companies',
        },
        name: {
            type: String,
            default: '',
            required: [true, 'Name is required'],
        },
        logo: {
            url: {
                type: String,
                default: '',
                required: [true, 'Logo is required'],
            },
        },
        coverUrl: {
            type: String,
            default: '',
            required: [true, 'Cover Url is required'],
        },
        about: {
            type: String,
            default: '',
            required: [true, 'Description is required'],
        },
        rating: {
            type: Number,
            default: 0,
        },
        campaigns: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'campaigns',
            },
        ],
        subscribedBy: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'influencers',
            },
        ],
    },
    { timestamps: true }
);

const Brand = mongoose.model('brand', BrandSchema);

module.exports = Brand;
