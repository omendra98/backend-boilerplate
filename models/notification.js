const mongoose = require('mongoose');

const notificationSchema = new mongoose.Schema({
    user: {
        userType: {
            type: String,
            required: [true, 'User Type is required'],
        },
        userId: {
            type: mongoose.Schema.Types.ObjectId,
        },
    },
    title: {
        type: String,
        required: [true, 'Notification Title is required'],
    },
    description: {
        type: String,
        required: [true, 'Notification Description is required'],
    },
    type: {
        type: String,
        required: [true, 'Notification Type is required'],
    },
    redirectUrl: {
        type: String,
        required: [true, 'RedirectUrl is required'],
    },
    read: {
        type: Boolean,
        default: false,
    },
});

const Notification = mongoose.model('notification', notificationSchema);

module.exports = Notification;
