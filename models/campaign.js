const mongoose = require('mongoose');

const campaignSchema = new mongoose.Schema({
    brand: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'brands',
    },
    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'companies',
    },
    createdOn: {
        type: Date,
        default: null,
    },
    lastEdittedOn: {
        type: Date,
        default: null,
    },
    activatedOn: {
        type: Date,
        default: null,
    },
    status: {
        type: String,
        default: 'Draft',
    },
    brief: {
        objective: {
            type: String,
            default: '',
        },
        title: {
            type: String,
            default: '',
        },
        brief: {
            type: String,
            default: '',
        },
        type: {
            type: String,
            default: '',
        },
        pageUrl: {
            type: String,
            default: '',
        },
        caption: {
            type: String,
            default: '',
        },
        coverPhoto: {
            url: {
                type: String,
                default: '',
            },
        },
        product: {
            providedBy: {
                type: String,
                default: '',
            },
            optionalDetails: {
                type: String,
                default: '',
            },
        },
    },
    criteria: {
        gender: {
            type: String,
            default: '',
        },
        city: {
            type: String,
            default: '',
        },
        age: {
            min: {
                type: Number,
                default: null,
            },
            max: {
                type: Number,
                default: null,
            },
        },
        followersCount: {
            min: {
                type: Number,
                default: null,
            },
            max: {
                type: Number,
                default: null,
            },
        },
        categories: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'categories',
            },
        ],
        moodboard: {
            type: Array,
            default: [],
        },
        description: {
            type: String,
            default: '',
        },
        dos: {
            type: Array,
            default: [],
        },
        donts: {
            type: Array,
            default: [],
        },
        hashtags: {
            type: Array,
            default: [],
        },
        house: {
            type: Array,
            default: [],
        },
    },
    budget: {
        type: Number,
        default: '',
    },
    startDate: {
        type: Date,
        default: null,
    },
    endDate: {
        type: Date,
        default: null,
    },
    applicantCount: {
        type: Number,
        default: 0,
    },
});

const Campaign = mongoose.model('campaign', campaignSchema);

module.exports = Campaign;
