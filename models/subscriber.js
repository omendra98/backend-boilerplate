const mongoose = require('mongoose');

const subscriberSchema = new mongoose.Schema({
    fcmToken: {
        type: String,
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
    },
    userType: {
        type: String,
    },
});

const Subscription = mongoose.model('subscription', subscriberSchema);

module.exports = Subscription;
