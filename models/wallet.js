const mongoose = require('mongoose');

const WalletSchema = new mongoose.Schema(
    {
        totalEarnings: {
            type: Number,
            default: 0,
        },
        moneyToBeWithdrawn: {
            type: Number,
            default: 0,
        },
    },
    { timestamps: true }
);

const Wallet = mongoose.model('wallet', WalletSchema);

module.exports = Wallet;
