const mongoose = require('mongoose');

const transactionSchema = new mongoose.Schema({
    influencer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'influencers',
    },
    type: {
        type: String,
        default: 'credit',
    },
    amount: {
        type: Number,
    },
    entity: {
        entityType: {
            type: String,
            default: 'Campaign',
        },
        entityId: {
            type: mongoose.Schema.Types.ObjectId,
        },
    },
});

const Transaction = mongoose.model('transaction', transactionSchema);

module.exports = Transaction;
