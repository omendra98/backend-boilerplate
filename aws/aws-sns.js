const AWS = require('aws-sdk');

const config = require('../config');

// Update SNS service credentials.
AWS.config.update({
    accessKeyId: config.ACCESS_KEY_ID,
    secretAccessKey: config.SECRET_ACCESS_KEY,
    region: config.REGION,
});

const aws = new AWS.SNS({ apiVersion: '2010-03-31' });

module.exports = aws;
