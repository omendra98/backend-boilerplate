const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

const config = require('../config');

// update settings
AWS.config.update({
    secretAccessKey: config.S3_SECRET_ACCESS_KEY,
    accessKeyId: config.S3_ACCESS_KEY_ID,
    region: config.S3_REGION,
});

const s3 = new AWS.S3();

// const fileFilter = (req, file, cb) => {
//     if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
//         cb(null, true);
//     } else {
//         console.log(file.mimetype);
//         cb(new Error('Invalid Mime Type, only JPEG and PNG'), false);
//     }
// };

const uploadPublicImage = multer({
    // fileFilter: fileFilter,
    storage: multerS3({
        s3: s3,
        bucket: 'famstar',
        acl: 'public-read',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        storageClass: 'REDUCED_REDUNDANCY',
        metadata: function (req, file, cb) {
            cb(null, { fieldName: file.fieldname });
        },
        key: function (req, file, cb) {
            cb(null, Date.now().toString());
        },
    }),
});

var uploadPrivateImage = multer({
    storage: multerS3({
        s3: s3,
        bucket: 'famstar',
        acl: 'authenticated-read',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        serverSideEncryption: 'AES256',
        storageClass: 'REDUCED_REDUNDANCY',
        metadata: function (req, file, cb) {
            cb(null, { fieldName: file.fieldname });
        },
        key: function (req, file, cb) {
            cb(null, Date.now().toString());
        },
    }),
});

module.exports = { uploadPublicImage, uploadPrivateImage };
