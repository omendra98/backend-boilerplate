const router = require('express').Router();
const { body } = require('express-validator');

const notificationMiddlewares = require('../../controllers/notification/notification');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.get('', authMiddleware(null), notificationMiddlewares.getNotifications);

router.put('/mark-read', authMiddleware(null), notificationMiddlewares.markReadNotification);

router.post(
    '/subscribe',
    authMiddleware(null),
    [body('userType').trim().isString(), body('fcmToken').trim().isString()],
    notificationMiddlewares.subscribe
);

router.post('/test', authMiddleware(null), notificationMiddlewares.test);

module.exports = router;
