const router = require('express').Router();
const { body, query } = require('express-validator');

const applicantsMiddlewares = require('../../controllers/company/campaign-applicants');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.get('/status/:status', authMiddleware('company'), [query('campaignId').isMongoId()], applicantsMiddlewares.getCampaignApplicants);

router.post(
    '/',
    authMiddleware('company'),
    [body('applicantId').isMongoId(), body('updateStatus').trim().isLength({ min: 6, max: 9 })],
    applicantsMiddlewares.updateStatus
);

module.exports = router;
