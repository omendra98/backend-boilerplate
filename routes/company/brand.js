const router = require('express').Router();
const { body } = require('express-validator');

const brandMiddlewares = require('../../controllers/company/brand');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.post(
    '/brand/create',
    authMiddleware('company'),
    [
        body('name').trim().isLength({ min: 4 }),
        body('logo').trim().isURL(),
        body('about').trim().isLength({ min: 8 }),
        body('coverUrl').trim().isString(),
    ],
    brandMiddlewares.createBrand
);

router.put(
    '/brand/edit',
    authMiddleware('company'),
    [
        body('id').trim(),
        body('name').trim().isString(),
        body('logo').trim().isString(),
        body('about').trim().isString(),
        body('coverUrl').trim().isString(),
    ],
    brandMiddlewares.editBrand
);

router.get('/brand', authMiddleware('company'), brandMiddlewares.viewBrands);

router.get('/brand/:id', authMiddleware('company'), brandMiddlewares.viewBrand);

module.exports = router;
