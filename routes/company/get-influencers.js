const router = require('express').Router();
const { query } = require('express-validator');

const getInfluencersMiddlewares = require('../../controllers/company/get-influencers');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.get('/get-influencer', authMiddleware('company'), [query('influencer').isMongoId()], getInfluencersMiddlewares.getInfluencer);

router.get('/get-influencers', authMiddleware('company'), getInfluencersMiddlewares.getInfluencers);

module.exports = router;
