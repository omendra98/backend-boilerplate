const router = require('express').Router();
const { body } = require('express-validator');

const brandProfileMiddlewares = require('../../controllers/company/profile');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.get('/get-profile', authMiddleware('company'), brandProfileMiddlewares.getProfile);

router.put(
    '/edit-profile',
    authMiddleware('company'),
    [body('mobile').trim().isMobilePhone('any', { strictMode: true }), body('fullName').trim().isLength({ min: 4 })],
    brandProfileMiddlewares.editProfile
);

module.exports = router;
