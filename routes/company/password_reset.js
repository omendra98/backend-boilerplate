const router = require('express').Router();
const { body } = require('express-validator');

const brandPasswordResetMiddlewares = require('../../controllers/company/password-reset');

router.post('/recover', [body('email').isEmail().withMessage('Enter a valid email address')], brandPasswordResetMiddlewares.recover);

router.get('/reset/:token', brandPasswordResetMiddlewares.reset);

router.post(
    '/reset/:token',
    [
        body('password').not().isEmpty().isLength({ min: 8 }).withMessage('Must be atleast 8 characters long'),
        body('confirmPassword', 'Passwords do not match').custom((value, { req }) => value === req.body.password),
    ],
    brandPasswordResetMiddlewares.resetPassword
);

module.exports = router;
