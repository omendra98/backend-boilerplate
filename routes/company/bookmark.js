const router = require('express').Router();
const { body } = require('express-validator');

const brandBookmarkMiddlewares = require('../../controllers/company/bookmark-influencer');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.put('/bookmarks/add', authMiddleware('company'), [body('influencerId').isMongoId()], brandBookmarkMiddlewares.bookmarkInfluencer);

module.exports = router;
