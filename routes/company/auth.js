const express = require('express');
const { body } = require('express-validator');
const router = express.Router();

const brandAuthMiddlewares = require('../../controllers/company/auth');

router.post(
    '/register',
    [
        body('email').isEmail().normalizeEmail().isLength({ min: 8 }),
        body('password').trim().isLength({ min: 8 }),
        body('name').trim().isLength({ min: 4 }),
        body('mobile').trim().isMobilePhone('any', { strictMode: true }),
        body('accountHolderName').trim().isLength({ min: 4 }),
    ],
    brandAuthMiddlewares.createUser
);

router.get('/register/verify/:token', brandAuthMiddlewares.verifyUser);

router.post('/login', [body('email').isEmail().normalizeEmail(), body('password').trim().isLength({ min: 8 })], brandAuthMiddlewares.login);

module.exports = router;
