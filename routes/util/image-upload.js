const router = require('express').Router();
const multer = require('multer');
const { uploadPublicImage } = require('../../aws/aws-s3');

const uploadFile = uploadPublicImage.single('file');

// function to upload an image to s3 bucket
router.post('/images/upload', (req, res) => {
    uploadFile(req, res, (err) => {
        if (err) return res.status(500).send({ error: true, message: err.message });
        return res.status(200).send({ error: false, data: { url: req.file.location } });
    });
});

module.exports = router;
