const router = require('express').Router();

const getLocationsMiddleware = require('../../controllers/util/get-locations');
const authMiddleware = require('../../controllers/auth/auth-middleware');

router.get('/get-locations', authMiddleware(null), getLocationsMiddleware.getLocations);

module.exports = router;
