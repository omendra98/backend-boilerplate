const router = require('express').Router();

const influencerGetBrandsMiddlewares = require('../../../controllers/influencer/brand/get-brand');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

router.get('/get-brands', authMiddleware('influencer'), influencerGetBrandsMiddlewares.getAllBrands);

module.exports = router;
