const router = require('express').Router();
const { body, query } = require('express-validator');

const influencerBookmarkBrandMiddlewares = require('../../../controllers/influencer/brand/bookmark-brand');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

router.get('/get-bookmarked-brands', authMiddleware('influencer'), influencerBookmarkBrandMiddlewares.getBookmarkedBrands);

router.post('/bookmark-brand', authMiddleware('influencer'), [body('id').isMongoId()], influencerBookmarkBrandMiddlewares.bookmarkBrand);

router.delete(
    '/remove-bookmarked-brand',
    authMiddleware('influencer'),
    [query('id').isMongoId()],
    influencerBookmarkBrandMiddlewares.removeBookmarkedBrand
);

module.exports = router;
