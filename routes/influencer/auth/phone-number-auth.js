const router = require('express').Router();
const { body } = require('express-validator');

const influencerAuthMiddlewares = require('../../../controllers/influencer/auth/phone-number-auth');

router.post(
    '/signin-phone-number',
    [body('phoneNumber').trim().isMobilePhone('any', { strictMode: true })],
    influencerAuthMiddlewares.signinPhoneNumber
);

router.post(
    '/verify-phone-number',
    [
        body('phoneNumber').trim().isMobilePhone('any', { strictMode: true }),
        body('otpToken').trim().isNumeric().isLength({ min: 6, max: 6 }),
    ],
    influencerAuthMiddlewares.verifyPhoneNumber
);

module.exports = router;
