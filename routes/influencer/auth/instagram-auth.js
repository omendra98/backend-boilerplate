const router = require('express').Router();
const { body } = require('express-validator');

const influencerAuthMiddlewares = require('../../../controllers/influencer/auth/instagram-auth.js');
const authMiddleware = require('../../../controllers/auth/auth-middleware');

router.post('/signin-instagram', [body('code').trim().isLength({ min: 1, max: undefined })], influencerAuthMiddlewares.signinInstagram);

router.post(
    '/add-instagram',
    authMiddleware('influencer'),
    [body('code').trim().isLength({ min: 1, max: undefined })],
    influencerAuthMiddlewares.addInstagram
);

module.exports = router;
