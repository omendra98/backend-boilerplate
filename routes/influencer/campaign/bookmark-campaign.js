const router = require('express').Router();
const { body, query } = require('express-validator');

const influencerBookmarkCampaignMiddlewares = require('../../../controllers/influencer/campaign/bookmark-campaign');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

router.get('/get-bookmarked-campaigns', authMiddleware('influencer'), influencerBookmarkCampaignMiddlewares.getBookmarkedCampaigns);

router.post(
    '/bookmark-campaign',
    authMiddleware('influencer'),
    [body('id').isMongoId()],
    influencerBookmarkCampaignMiddlewares.bookmarkCampaign
);

router.delete(
    '/remove-bookmarked-campaign',
    authMiddleware('influencer'),
    [query('id').isMongoId()],
    influencerBookmarkCampaignMiddlewares.removeBookmarkedCampaign
);

module.exports = router;
