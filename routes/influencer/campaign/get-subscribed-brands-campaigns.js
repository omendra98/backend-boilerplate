const router = require('express').Router();

const influencerGetSubscribedBrandsCampaignsMiddlewares = require('../../../controllers/influencer/campaign/get-subscribed-brands-campaigns');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

router.get(
    '/get-subscribed-brands-campaigns',
    authMiddleware('influencer'),
    influencerGetSubscribedBrandsCampaignsMiddlewares.getSubscribedBrandsCampaigns
);

module.exports = router;
