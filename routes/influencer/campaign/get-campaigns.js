const router = require('express').Router();
const { param, query } = require('express-validator');

const influencerGetCampaignsMiddlewares = require('../../../controllers/influencer/campaign/get-influencer-campaigns');
const influencerCampaignMiddlewares = require('../../../controllers/influencer/campaign/get-campaigns');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

router.get(
    '/get-campaigns/:status',
    authMiddleware('influencer'),
    [param('status').isIn(['Applied', 'Rejected', 'Approved'])],
    influencerGetCampaignsMiddlewares.getInfluencerCampaigns
);

router.get('/get-campaigns-by-categories/filter', authMiddleware('influencer'), influencerCampaignMiddlewares.getCampaignsByCategories);

router.get('/get-campaigns-for-influencer/filter', authMiddleware('influencer'), influencerCampaignMiddlewares.getAllCampaigns);

router.get('/search-campaign', authMiddleware('influencer'), influencerCampaignMiddlewares.searchCampaign);

router.get(
    '/get-campaigns-by-brand',
    authMiddleware('influencer'),
    [query('id').isMongoId()],
    influencerCampaignMiddlewares.getCampaignsByBrandId
);

module.exports = router;
