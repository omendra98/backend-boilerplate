const router = require('express').Router();
const { body } = require('express-validator');

const influencerAddBankAccountMiddlewares = require('../../../controllers/influencer/wallet/add-bank-account');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

router.post(
    '/add-bank-account',
    authMiddleware('influencer'),
    [body('accountHolderName').trim().isString(), body('ifsc').trim().isString(), body('accountNumber').trim().isString()],
    influencerAddBankAccountMiddlewares.addBankAccount
);

module.exports = router;
