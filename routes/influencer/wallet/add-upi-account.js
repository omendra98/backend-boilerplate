const router = require('express').Router();
const { body } = require('express-validator');

const influencerAddUPIAccountMiddlewares = require('../../../controllers/influencer/wallet/add-upi-account');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

router.post(
    '/add-upi-account',
    authMiddleware('influencer'),
    [body('upiId').trim().isString()],
    influencerAddUPIAccountMiddlewares.addUPIAccount
);

module.exports = router;
