const router = require('express').Router();

const searchMiddlewares = require('../../controllers/influencer/search');

router.get('/search', searchMiddlewares.search);

module.exports = router;
