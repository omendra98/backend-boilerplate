const router = require('express').Router();
const { body } = require('express-validator');

const influencerProfileMiddlewares = require('../../controllers/influencer/profile');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.get('/get-profile', authMiddleware('influencer'), influencerProfileMiddlewares.getProfile);

router.post(
    '/save-profile',
    authMiddleware('influencer'),
    [
        body('name').trim().isString(),
        body('dob').isDate(),
        body('gender').trim().isIn(['male', 'female']),
        body('city').trim(),
        body('country').trim(),
        body('email').trim().isEmail(),
        body('phoneNumber').trim().isMobilePhone(),
    ],
    influencerProfileMiddlewares.saveProfile
);

router.get('/get-categories', authMiddleware('influencer'), influencerProfileMiddlewares.getCategories);

router.post('/save-categories', authMiddleware('influencer'), [body('categoryIds').isArray()], influencerProfileMiddlewares.saveCategories);

module.exports = router;
