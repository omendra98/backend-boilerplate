const router = require('express').Router();
const { query } = require('express-validator');

const influencerGetCampaignsByStatusMiddlewares = require('../../../controllers/influencer/applicant/get-campaigns-by-status');

const authMiddleware = require('../../../controllers/auth/auth-middleware');

router.get(
    '/get-campaigns-by-status',
    authMiddleware('influencer'),
    query('status').isString(),
    influencerGetCampaignsByStatusMiddlewares.getCampaignsByStatus
);

module.exports = router;
