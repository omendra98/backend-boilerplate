const router = require('express').Router();
const { body } = require('express-validator');

const categoryMiddlewares = require('../../controllers/admin/category');

router.get('/get-all-categories', categoryMiddlewares.getAllCategories);

router.post(
    '/add-category',
    [body('name').isString().trim().isAlpha(), body('icon').isString().trim().isURL()],
    categoryMiddlewares.addCategory
);

router.put(
    '/update-category',
    [
        body('id').isMongoId(),
        body('name').isString().trim().isAlpha(),
        body('description').isString().trim(),
        body('icon').isString().trim().isURL(),
    ],
    categoryMiddlewares.updateCategory
);

router.delete('/remove-category', [body('categoryId').isMongoId()], categoryMiddlewares.removeCategory);

module.exports = router;
