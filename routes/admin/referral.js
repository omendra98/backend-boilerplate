const router = require('express').Router();
const { body } = require('express-validator');

const referralMiddlewares = require('../../controllers/admin/referral');

router.get('/get-referral-amount-for-referrer', referralMiddlewares.getReferralAmountForReferrer);

router.get('/get-referral-amount-for-referred', referralMiddlewares.getReferralAmountForReferred);

router.get('/get-referral-percentage', referralMiddlewares.getReferralPercentage);

router.get('/get-referral-max-amount', referralMiddlewares.getReferralMaxAmount);

router.put(
    '/save-referral-amount-for-referrer',
    [body('referralAmountForReferrer').isInt()],
    referralMiddlewares.saveReferralAmountForReferrer
);

router.put(
    '/save-referral-amount-for-referred',
    [body('referralAmountForReferred').isInt()],
    referralMiddlewares.saveReferralAmountForReferred
);

router.put('/save-referral-percentage', [body('referralPercentage').isFloat()], referralMiddlewares.saveReferralPercentage);

router.put('/save-referral-max-amount', [body('referralMaxAmount').isInt()], referralMiddlewares.saveReferralMaxAmount);

module.exports = router;
