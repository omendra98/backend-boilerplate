const router = require('express').Router();
const { body, param } = require('express-validator');

const campaignMiddlewares = require('../../controllers/campaign');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.get('/get-campaigns-by-status/:status', authMiddleware('company'), campaignMiddlewares.getCampaignsByStatus);

router.get('/get-campaigns', authMiddleware('company'), campaignMiddlewares.getCampaigns);

router.get('/brands/:id', [param('id').isMongoId()], authMiddleware('company'), campaignMiddlewares.getCampaignsByBrandId);

router.get('/:id', authMiddleware('company'), [param('id').isMongoId()], campaignMiddlewares.getCampaign);

router.post('/upsert', authMiddleware('company'), campaignMiddlewares.upsertCampaign);

router.delete(
    '/delete',
    authMiddleware('company'),
    [body('brandId').isMongoId(), body('campaignId').isMongoId()],
    campaignMiddlewares.deleteCampaign
);

module.exports = router;
