const router = require('express').Router();
const { body } = require('express-validator');

const { uploadPublicImage } = require('../../aws/aws-s3');

const applyCampaignMiddlewares = require('../../controllers/campaign/apply-campaign');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.post(
    '/apply-campaign',
    authMiddleware('influencer'),
    uploadPublicImage.array('files', 100),
    [body('campaignId').isMongoId(), body('biddingAmount').isInt()],
    applyCampaignMiddlewares.applyCampaign
);

router.put(
    '/post-link',
    authMiddleware('influencer'),
    [body('postLink').isString(), body('campaignId').isMongoId()],
    applyCampaignMiddlewares.updateLink
);

module.exports = router;
