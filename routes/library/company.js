const router = require('express').Router();
const { body, param, query } = require('express-validator');

const companyLibraryMiddlewares = require('../../controllers/library/company');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.get('/get-contents/:tag', authMiddleware('company'), companyLibraryMiddlewares.viewAllContents);

router.put('/bookmark-content', authMiddleware('company'), [body('contentId').isMongoId()], companyLibraryMiddlewares.bookmarkContent);

router.put('/remove-bookmark', authMiddleware('company'), [body('contentId').isMongoId(), companyLibraryMiddlewares.deleteBookmarkContent]);

module.exports = router;
