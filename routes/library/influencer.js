const router = require('express').Router();
const { body, param } = require('express-validator');

const { uploadPublicImage } = require('../../aws/aws-s3');

const influencerLibraryMiddlewares = require('../../controllers/library/influencer');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.post(
    '/create-content',
    authMiddleware('influencer'),
    uploadPublicImage.single('file'),
    [body('price').trim().isNumeric(), body('caption').trim().isString()],
    influencerLibraryMiddlewares.createContent
);

router.put(
    '/edit-content',
    authMiddleware('influencer'),
    [body('price').trim().isNumeric(), body('caption').trim().isString()],
    influencerLibraryMiddlewares.editContent
);

router.get('/view-content/:id', authMiddleware('influencer'), [param('id').isMongoId()], influencerLibraryMiddlewares.viewContent);

router.get('/view-contents', authMiddleware('influencer'), influencerLibraryMiddlewares.viewContents);

router.delete('/delete-content', authMiddleware('influencer'), influencerLibraryMiddlewares.deleteContent);

module.exports = router;
