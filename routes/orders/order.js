const router = require('express').Router();
const { body, query } = require('express-validator');

const orderMiddlewares = require('../../controllers/orders/orders');

const authMiddleware = require('../../controllers/auth/auth-middleware');

router.post(
    '/create-order',
    authMiddleware('company'),
    [body('entity').trim().isString(), body('entityId').isMongoId(), body('currency').trim().isLength({ max: 3, min: 3 })],
    orderMiddlewares.createOrder
);

router.get('/get-orders', authMiddleware('company'), orderMiddlewares.getOrders);

router.get('/get-order', authMiddleware('company'), [query('orderId').isMongoId()], orderMiddlewares.getOrder);

router.get(
    '/check-order',
    authMiddleware('company'),
    [query('entityType').isString(), query('entityId').isMongoId()],
    orderMiddlewares.checkOrder
);

module.exports = router;
